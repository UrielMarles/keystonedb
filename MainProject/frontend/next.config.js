/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  generateEtags: false,
  env: {
    enviroment: "desarrollo", // "testing" "produccion" "desarrollo" // valores posibles
    addressNavegador: "http://localhost:5034",
    captchaKey: "6LdnnhMUAAAAAHn7A4-TV34rglE4kUXSwL8JqYVo",
    disabledBtnVotar: "true", // "false" o "true" en string
  },
  webpackDevMiddleware: (config) => {
    config.watchOptions = {
      poll: 800,
      aggregateTimeout: 300,
    };
    return config;
  },
  webpack: (config, { isServer }) => {
    if (!isServer) {
      config.resolve.fallback.fs = false;
    }

    return config;
  },
  images: {
    loader: "akamai",
    path: "",
  },
};

module.exports = nextConfig;
