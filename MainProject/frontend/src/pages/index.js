import React, { useEffect, useState } from 'react';

export async function getServerSideProps() {
  
    // Realiza la solicitud para obtener los datos del país con el ID proporcionado
    const response = await fetch(`http://project-keystone-api:5000/countries/1`);
    const response2 = await fetch('http://project-keystone-api:5000/dummyTable/list', {method: 'POST', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify({ labelValue: [ 'createdAt', 'id' ]})});
    const countryData = await response.json();
    const dummyData = await response2.json();
    console.log(dummyData)
  
    // Pasa los datos del país como prop al componente
    return {
      props: {
        dummy: dummyData,
        country: countryData,
      },
    };
  };

export default function MostrarDummyTableClient({ country,dummy })
  {
    return (<div><h1>{country.description}</h1>
    <h2>{dummy[0].label}</h2> 
    </div>);

  };