function MyApp({ Component, pageProps }) {
    // Tu lógica de componente aquí
    return <Component {...pageProps} />;
  }
  
  export default MyApp;