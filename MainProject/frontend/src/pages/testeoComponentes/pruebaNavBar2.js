import { styled } from '@mui/system';
import React, { useState, useEffect } from 'react';
import { Drawer, List, ListItem, ListItemText, Collapse, IconButton } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Box from '@mui/material/Box';
import AddIcon from '@mui/icons-material/Add';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';

const StyledSidebar = styled('div')({
  position: 'fixed',
  left: 0,
  top: 0,
  height: '100%',
  width: '50px',
  alignItems: 'center', 
  backgroundColor: '#000000', // Set to black color
  color: (theme) => theme.palette.primary.contrastText,
  zIndex:2
});




// StyledDrawer para ajustar el marginLeft y la transición
const StyledDrawer = styled(Drawer)(
  {
    '& .MuiDrawer-paper': {
      backgroundColor: '#000000', // Color de fondo del Drawer
      color: '#ffffff', // Color del texto dentro del Drawer
    },
  color: '#000000',
  transition: 'margin-left 0.3s ease',
  zIndex:1
});


const Pagina = () => {
  const [drawerOpen, setDrawerOpen] = useState(false);
  const [openSections, setOpenSections] = useState({});
  const [iconosVisibles,setIconosVisibles] = useState ({})

  const iconosNombres = [
    "Add","Menu"
  ]
    
  const [iconos, setIconos] = useState([]);
  useEffect(() => {
    // Función asincrónica para cargar los iconos dinámicamente
    const cargarIconos = async () => {
      const iconosImportados = await Promise.all(
        iconosNombres.map(async (nombre) => {
          // Importa el icono dinámicamente
          const modulo = await import(`@mui/icons-material/${nombre}`);
          // Accede al icono en el módulo importado
          return modulo.default;
        })
      );
      // Actualiza el estado con los iconos importados
      setIconos(iconosImportados);
    };

    // Llama a la función para cargar los iconos
    cargarIconos();
  }, [iconosNombres]); 


  useEffect(()=>{ setIconosVisibles({
    "Proyectos":true,
    "subMenu":false,
    "opcionFinal":false,
    "SubopcionB":false,
    "Clientes":true
  });}
  ,[])


  const handleDrawerClose = () => {
    setDrawerOpen(false);
  };

  const handleSectionToggle = (section) => {
    setOpenSections((prevOpenSections) => ({
      ...prevOpenSections,
      [section]: !prevOpenSections[section],
    }));
  };
  return (
    <div>
      <StyledSidebar>
      <Box display="flex" flexDirection="column" sx={{ background: 'black', paddingLeft: "3px",paddingTop:"5px",paddingRight:"4px"}}>
      <IconButton
        sx={{
          backgroundColor: 'gray',
          border: '1px solid #ccc', // Añade padding a la izquierda
          '& .MuiIconButton-label': {
            fontSize: '12px', // Ajusta el tamaño del botón (el círculo alrededor del icono)
          },
        }}
         onClick={() => drawerOpen?setDrawerOpen(false):setDrawerOpen(true)}>
          {drawerOpen?(<ArrowBackIcon sx={{ color: 'white' }}/>):(<ArrowForwardIcon sx={{ color: 'white' }}/>)}
      </IconButton>
      <IconButton
      sx={{
          backgroundColor: 'gray',
          border: '1px solid #ccc', // Añade padding a la izquierda
          '& .MuiIconButton-label': {
            fontSize: '12px', // Ajusta el tamaño del botón (el círculo alrededor del icono)
          },
        }}>
          {}
      </IconButton>
  
        </Box>
      </StyledSidebar>
      <StyledDrawer anchor="left" open={drawerOpen} onClose={handleDrawerClose}>
      <Box display="flex" flexDirection="row" sx={{ background: 'black', paddingLeft: "40px",paddingTop:"35px"}}>
      <List>
          <ListItem button onClick={() => handleSectionToggle('proyectos')}>
            <ListItemText primary="Proyectos" />
            {openSections['proyectos'] ? <ExpandMoreIcon /> : null}
          </ListItem>

          <Collapse in={openSections['proyectos']} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              <ListItem button onClick={() => handleSectionToggle('submenu')}>
                <ListItemText primary="Submenú" />
                {openSections['submenu'] ? <ExpandMoreIcon /> : null}
              </ListItem>
              <Collapse in={openSections['submenu']} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                  <ListItem button onClick={handleDrawerClose}>
                    <ListItemText primary="Opción final" />
                  </ListItem>
                </List>
              </Collapse>

              <ListItem button onClick={handleDrawerClose}>
                <ListItemText primary="Subopción B" />
              </ListItem>
            </List>
          </Collapse>

          <ListItem button onClick={() => handleSectionToggle('clientes')}>
            <ListItemText primary="Clientes" />
            {openSections['clientes'] ? <ExpandMoreIcon /> : null}
            <Collapse in={openSections['clientes']} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                  <ListItem button onClick={handleDrawerClose}>
                    <ListItemText primary="clienteOpcion" />
                  </ListItem>
                </List>
              </Collapse>
          </ListItem>
        </List>
      </Box>
      </StyledDrawer>
    </div>
  );
};

export default Pagina;
