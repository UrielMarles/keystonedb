// Importa el componente ComboBox desde el archivo especificado
import ComboBox from "components/Forms/comboBox";
// Importa las constantes SERVER_FETCH_URL y CLIENT_FETCH_URL desde el archivo de configuración http
import { SERVER_FETCH_URL, CLIENT_FETCH_URL } from 'config/http';

// Define las opciones para el componente ComboBox
const options = {
    Data :[
        {"label":"jfidsofjsd","value":"1"},
        {"label":"opcionPersonalizada2","value":"2"},
        {"label":"aaa","value":"3"}],
    Source:{
        "Route":`${CLIENT_FETCH_URL}dummyTable/list`,
        "Params":{
            "labelValue":["createdAt","id"]
          }
    }
}


export default function Pagina(){
    return(<div><ComboBox opts={options}/></div>)
}

