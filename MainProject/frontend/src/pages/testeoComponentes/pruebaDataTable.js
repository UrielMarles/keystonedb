// Importa el componente GenericTable desde el archivo especificado
import GenericTable from "components/Forms/dataTable";
// Importa las constantes SERVER_FETCH_URL y CLIENT_FETCH_URL desde el archivo de configuración http
import { SERVER_FETCH_URL, CLIENT_FETCH_URL } from 'config/http';

// Configuración de datos para la tabla
const options = {
  // Datos de ejemplo para la tabla
  Data: [{
      "createdAt": "05/12/2023 08:41",
      "id": 1,
      "name": "ESCRITO"
    },
    {
      "createdAt": "05/12/2023 08:41",
      "id": 2,
      "name": "A"
    },
    {
      "createdAt": "05/12/2023 08:41",
      "id": 3,
      "name": "MANOOO"
    },
    {
      "createdAt": "05/12/2023 08:41",
      "id": 4,
      "name": "WAAAA"
    },
    {
      "createdAt": "05/12/2023 08:41",
      "id": 5,
      "name": "TERRIBLE"
    }
  ],
  Source: {
    "Route": `${CLIENT_FETCH_URL}/roles/list`,
    "Params": {
      "columns": ["createdAt", "id", "name"]
    }
  }
}

// Componente principal de la aplicación
export default function App() {
  return (
    <div className="App">
      {/* Proporciona la configuración específica de la tabla como prop */}
      <GenericTable opts={options} />
    </div>
  );
}
