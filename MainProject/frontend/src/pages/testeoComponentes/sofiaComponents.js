import Select from "components/Forms/select";
import SwitchLabels from "components/Forms/switch";
import List from "components/Forms/transferList";
import Slide from "components/Forms/slider";
import Avatar from "components/Forms/avatar";
import { SERVER_FETCH_URL, CLIENT_FETCH_URL } from 'config/http';

//Personalización
const optionsSwitch = {label:"Boton Bonito", color: "secondary", defaultChecked: false,}
const optionsSelect ={ 
    Data:[
        {"label":"Coca-Cola", "value":1},
        {"label":"Fanta", "value":2},
        {"label":"Sprite", "value":3}
    ],
    View:{
    label:"Gaseosas", 
    width:500,
    size: "large"
}, 
    Source: {
        "Route": `${CLIENT_FETCH_URL}roles/list`,
        "Params": {
            "labelValue": ["name", "id"]
        }
    }
}
const optionsList ={
    "leftPosition":[
        {value:0, label:"Lechuga"},
        {value:1, label:"Tomate"},
        {value:2, label:"Cebolla"},
        {value:3, label:"Pepino"}
    ],
    "rightPosition":[
        {value:4, label:"Palta"},
        {value:5, label:"Morron"},
        {value:6, label:"Papa"},
        {value:7, label:"Zanahoria"}
    ]
}
const optionsSlide ={
    Data : [
        {value: 0, label: "1%"},
        {value: 50, label: "50%"},
        {value: 100, label: "100%"},
    ],
    View:{
    "width": 1000,
    "height": 3,
    "size": "large",
    "color":"secondary",
    "track": false,
    "defaultValue": 10,
    }
}

//llamado de personalización
export default function Pagina(){
    //espaciado de hr
    const stylehr = { margin: "20px 0"}; 
    return(<div>
        <Avatar/>
        <hr style={stylehr}/>
        <SwitchLabels opts={optionsSwitch}/>
        <hr style={stylehr}/>
        <Select opts={optionsSelect}/>
        <hr style={stylehr}/>
        <List opts={optionsList}/>
        <hr style={stylehr}/>
        <Slide opts={optionsSlide}/>
        </div>)
}