import React, { useState } from 'react';
import { Drawer, List, ListItem, ListItemText, Collapse, IconButton } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

const NavigationBar = () => {
  const [drawerOpen, setDrawerOpen] = useState(false);
  const [openSections, setOpenSections] = useState({});

  const handleDrawerOpen = () => {
    setDrawerOpen(true);
  };

  const handleDrawerClose = () => {
    setDrawerOpen(false);
  };

  const handleSectionToggle = (section) => {
    setOpenSections((prevOpenSections) => ({
      ...prevOpenSections,
      [section]: !prevOpenSections[section],
    }));
  };

  return (
    <>
      <IconButton onClick={handleDrawerOpen} edge="start" color="inherit" aria-label="menu">
        <MenuIcon />
      </IconButton>

      <Drawer anchor="left" open={drawerOpen} onClose={handleDrawerClose}>
        <List>
          <ListItem button onClick={() => handleSectionToggle('proyectos')}>
            <ListItemText primary="Proyectos" />
            {openSections['proyectos'] ? <ExpandMoreIcon /> : null}
          </ListItem>

          <Collapse in={openSections['proyectos']} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              <ListItem button onClick={() => handleSectionToggle('submenu')}>
                <ListItemText primary="Submenú" />
                {openSections['submenu'] ? <ExpandMoreIcon /> : null}
              </ListItem>

              <Collapse in={openSections['submenu']} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                  <ListItem button onClick={handleDrawerClose}>
                    <ListItemText primary="Opción final" />
                  </ListItem>
                </List>
              </Collapse>

              <ListItem button onClick={handleDrawerClose}>
                <ListItemText primary="Subopción B" />
              </ListItem>
            </List>
          </Collapse>

          <ListItem button onClick={() => handleSectionToggle('clientes')}>
            <ListItemText primary="Clientes" />
            {openSections['clientes'] ? <ExpandMoreIcon /> : null}
          </ListItem>
        </List>
      </Drawer>
    </>
  );
};

export default NavigationBar;
