import Tabla from "components/Forms/TABLELOCA"

import { SERVER_FETCH_URL, CLIENT_FETCH_URL } from 'config/http';

// Configuración de datos para la tabla
const options = {
  // Datos de ejemplo para la tabla
  Data: [{
      "createdAt": "05/12/2023 08:41",
      "id": 1,
      "COLUMNA": "ESCRITO"
    },
    {
      "createdAt": "05/12/2023 08:41",
      "id": 2,
      "COLUMNA": "A"
    },
    {
      "createdAt": "05/12/2023 08:41",
      "id": 3,
      "COLUMNA": "MANOOO"
    },
    {
      "createdAt": "05/12/2023 08:41",
      "id": 4,
      "COLUMNA": "WAAAA"
    },
    {
      "createdAt": "05/12/2023 08:41",
      "id": 5,
      "COLUMNA": "TERRIBLE"
    },
    {
      "createdAt": "05/12/2023 08:41",
      "id": 6,
      "COLUMNA": "TERRIBLE2"
    },
    {
      "createdAt": "05/12/2023 08:41",
      "id": 7,
      "COLUMNA": "TERRIBLE3"
    },
    {
      "createdAt": "05/12/2023 08:41",
      "id": 8,
      "COLUMNA": "TERRIBLE4"
    },
    {
      "createdAt": "05/12/2023 08:41",
      "id": 9,
      "COLUMNA": "TERRIBLE5"
    }
  ],
  Source: {
    "Route": `${CLIENT_FETCH_URL}/roles/list`,
    "TableName": "roles",
    "Params": {
        "columns":["id","name","createdAt","deletedById","active"],
        "paging":{
          "itemsPerPage":5,
          "actualPage":1
        }
      }
  },
  Style:{
    "defaultPageSize": 5,
    "height":300,
    "width":700
  }
}
export default function pagina(){
    return (<Tabla opts={options}/>)
}