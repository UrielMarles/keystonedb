import * as React from 'react';
import Chip from '@mui/material/Chip';
import Stack from '@mui/material/Stack';
import configDefault from './config';

export default function BasicChips(props) {
    const config = Object.assign(configDefault,props)
  return (
    <Stack direction="row" spacing={1}>
      <Chip {...config}/>
      <Chip {...config} /*label="Chip Outlined" variant="outlined" *//>
    </Stack>
  );
}
