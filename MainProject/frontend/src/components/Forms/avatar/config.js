let config ={
    "spacing": 2,
    "src1" : "https://i.pinimg.com/564x/2d/51/0f/2d510f3a1f27bc1c43f6cd6566abc01c.jpg",
    "alt1" : "Venti",
    "src2" : "https://i.pinimg.com/564x/35/c1/43/35c1433e406eecb438402ae2d19993d1.jpg",
    "alt2" : "Raiden",
    "src3" : "https://i.pinimg.com/564x/11/5b/88/115b88984a312e4655256ac7df4b4b0e.jpg",
    "alt3" : "Nahida",
}

export default config;