import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Stack from '@mui/material/Stack';
import configDefault from './config';

export default function ImageAvatars(props) {
  const config = Object.assign({},configDefault, props.opts);
  return (
        <Stack direction="row" spacing={config.spacing}>
        <Avatar alt={config.alt1} src={config.src1} />
        <Avatar alt={config.alt2} src={config.src2} />
        <Avatar alt={config.alt3} src={config.src3} />
        </Stack>
  );
}