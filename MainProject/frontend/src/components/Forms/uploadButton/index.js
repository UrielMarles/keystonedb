import * as React from 'react';
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import configDefault from "./config.js"

const VisuallyHiddenInput = styled('input')({
    clip: 'rect(0 0 0 0)',
    clipPath: 'inset(50%)',
    height: 1,
    overflow: 'hidden',
    position: 'absolute',
    bottom: 0,
    left: 0,
    whiteSpace: 'nowrap',
    width: 1,
  });


  export default function InputFileUpload(props) {
    const config = Object.assign(configDefault,props)  
    
    return (
      <Button component="label" variant="contained" color='success' startIcon={<CloudUploadIcon />}>
        Suba aqui sus documentos
        <VisuallyHiddenInput type="file" />
      </Button>
    );
  }