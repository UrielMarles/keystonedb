import * as React from 'react';
import Box from '@mui/material/Box';
import MUITextField from '@mui/material/TextField';
import configDefault from "./config.js"

//importo el textfield de mui y el config.js

export default function BasicTextFields(props) { 
    //props son las propiedes que definimos
  
const config = Object.assign(configDefault,props.opts)
//const config son las propiedades basicas del objeto, asigna las propiedades por default y sino las del segundo archivo de configuración 
//
return (
      <MUITextField {...config} />
      //se pasan las configuraciones a travez del {config}, para que en config.js se pasen todos los parametros necesarios
      
  );
}