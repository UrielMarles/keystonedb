let config ={
    Data:[
        {"label":"Opcion 1", "value": 1},
        {"label":"Opcion 2", "value": 2},
        {"label":"Opcion 3", "value": 3},
        {"label":"Opcion 4", "value": 4},
    ],
    
    View:{
        "label":"Opciones Predeterminadas",
        "width" : 400,
        "size": "small"
    },
    Source:{
        Route:null,
        Params:{
            "columns": ["id", "nombre", "edad"],
            "paginacion": {
                "itemsXPagina": 50,
                "paginaActual": 1
            },
            "labelValue": ["name", "id"],
            "order": ['nombre asc', 'apellidodesc'],
            "filters": {
                "apellido": {
                    "conector": "and",
                    "condicion": "like",
                    "tipo": "string",
                    "valor": "%ponce%"
                },
                "nombre": {
                    "conector": "or",
                    "condicion": "=",
                    "valor": "sheila"
                }
            }
        }
    }
}

export default config;
