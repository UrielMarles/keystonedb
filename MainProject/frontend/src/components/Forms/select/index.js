import React, { useEffect, useState } from 'react';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MuiSelect from '@mui/material/Select' ;
import MenuItem from '@mui/material/MenuItem'
import configDefault from './config';

export default function Select({opts}) {
    const [config, setConfig] = useState(Object.assign(configDefault,opts));
    const [data, setData]= useState(null)

  // Efecto que se ejecuta al montar el componente
    useEffect(() => {
        if(config.Source.Route !== null){
        fetch(config.Source.Route, {method: 'POST', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(config.Source.Params)})
        .then(response => response.json())
        .then(data => {setData(data) ;setConfig({...config,...{"Data": data}})} 
    )}
    }, []);
    return (
        <FormControl style={{ width: config.View.width }} >
        <InputLabel id="demo-simple-select-label">{config.View.label}</InputLabel>
            <MuiSelect
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            label={config.View.label}
            value={config.View.selectedValue}
            size={config.View.size}
            style={{ width: config.View.width}}
            >
            <InputLabel id="demo-simple-select-label" ></InputLabel>
            {config.Data.map((option, index) => (
                <MenuItem key={index} value={option.label}>
                    {option.label}
                </MenuItem>
            ))}
            </MuiSelect>
        </FormControl>);
        } 
/*
<FormHelperText>Without label</FormHelperText>
import * as React from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

export default function BasicSelect() {
    const [age, setAge] = React.useState('');

    const handleChange = (event) => {
        setAge(event.target.value);
    };

    return (
        <Box sx={{ minWidth: 120 }}>
        <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Age</InputLabel>
            <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={age}
            label="Age"
            onChange={handleChange}
            >
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
            </Select>
        </FormControl>
        </Box>
    );
}
*/