let config ={
    Data : [
        {value: 0, label: "0"},
        {value: 10, label: "10"},
        {value: 20, label: "20"},
        {value: 30, label: "30"},
        {value: 40, label: "40"},
        {value: 50, label: "50"},
        {value: 60, label: "60"},
        {value: 70, label: "70"},
        {value: 80, label: "80"},
        {value: 90, label: "90"},
        {value: 100, label: "100"},
    ],
    View:{
    "width": 600,
    "height": 200,
    "size": "small",
    "color":"warning",
    "track": true,
    "defaultValue": 50,
    "label": "Barrita"
    },
/*
    Source:{
        Route:null,
        Params:{
            "columns": ["id", "nombre", "edad"],
            "paginacion": {
                "itemsXPagina": 50,
                "paginaActual": 1
            },
            "labelValue": ["nombre", "id"],
            "order": ['nombre asc', 'apellidodesc'],
            "filters": {
                "apellido": {
                    "conector": "and",
                    "condicion": "like",
                    "tipo": "string",
                    "valor": "%ponce%"
                },
                "nombre": {
                    "conector": "or",
                    "condicion": "=",
                    "valor": "sheila"
                }
            }
        }
    }
*/
}


export default config;