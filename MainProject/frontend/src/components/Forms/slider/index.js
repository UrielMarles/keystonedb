import * as React from 'react';
import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider';
import configDefault from './config'
import Typography from '@mui/material/Typography';


function valuetext(value) {
    return `${value}°C`;
}

export default function TrackFalseSlider(props) {
    const config = Object.assign({},configDefault, props.opts)
    return (
        <Box sx={{ width: config.View.width, height: config.View.height}}>
            <Typography id="input-slider" gutterBottom>{config.View.label}</Typography>
            <Slider
                track={config.track}
                aria-labelledby="track-false-slider"
                getAriaValueText={valuetext}
                defaultValue={config.View.defaultValue}
                marks={config.Data}
                size={config.View.size}
                color={config.View.color}
                step={config.View.step}
            />
        </Box>
    );
}
/*import * as React from 'react';
import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider';
import configDefault from './config'


function valuetext(value) {
    return `${value}°C`;    
}

export default function DiscreteSliderMarks(props) {
    const config = Object.assign({},configDefault, props.opts)
    return (
        <Box sx={{ width:config.width }}>
        <Slider
            track ={config.track}
            aria-label="Custom marks"
            defaultValue={20}
            getAriaValueText={valuetext}
            step={10}
            valueLabelDisplay="auto"
            marks={config.marks}
            color= {config.color}
            size={config.size}
        />
        </Box>
    );
}
*/