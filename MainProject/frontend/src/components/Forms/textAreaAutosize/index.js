import * as React from 'react';
import FormControlLabel from '@mui/material/FormControlLabel';
//FormControlLabel es un componente para poder ponerle etiquetas a los componentes
import { TextareaAutosize as BaseTextareaAutosize } from '@mui/base/TextareaAutosize';
//import { styled } from '@mui/system';
import configDefault from "./config.js"
//importo el text area de mui y le declaro el config.js


export default function Textarea(props) {
    const config = Object.assign(configDefault,props.opts)

  return <FormControlLabel {...config} control ={<BaseTextareaAutosize{...config}/>}/>;
}
