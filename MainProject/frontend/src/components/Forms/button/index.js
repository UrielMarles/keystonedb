import * as React from 'react';
import Stack from '@mui/material/Stack';
import MUIButton from '@mui/material/Button';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import configDefault from "./config.js"
//importo el boton de mui y el config.js


export default function Button(props) { 
  //props son las propiedes que definimos

  const config = Object.assign(configDefault,props)
  console.log(props)
    return (
         <MUIButton {...config}>{config.text}</MUIButton>
        );
}