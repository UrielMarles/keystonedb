import * as React from 'react';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import configDefault from "./config"

export default function RadioButtonsGroup(props) {
    const config = Object.assign(configDefault,props.opts)
  return (
    <FormControl>
      <FormLabel id="demo-radio-buttons-group-label">Genero</FormLabel>
      <RadioGroup
        aria-labelledby="demo-radio-buttons-group-label"
        defaultValue="female"
        name="radio-buttons-group"
      >
        <FormControlLabel value="Mujer" control={<Radio />} label="Mujer" />
        <FormControlLabel value="Hombre" control={<Radio />} label="Hombre" />
        <FormControlLabel value="Otros" control={<Radio />} label="Otros" />
      </RadioGroup>
    </FormControl>
  );
}