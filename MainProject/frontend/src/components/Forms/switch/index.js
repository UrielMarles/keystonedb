import * as React from 'react';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import configDefault from './config';


export default function SwitchLabels(props) {
    const config = Object.assign(configDefault,props.opts)
    return (
        <FormControlLabel {...config} control={<Switch {...config}/>} />
    );
}