let config ={
    "leftPosition" : [
        {id: 0, text: "Posicion 0"},
        {id: 1, text: "Posicion 1"},
        {id: 2, text: "Posicion 2"},
        {id: 3, text: "Posicion 3"},
    ],
    "rightPosition" : [
        {id: 4, text: "Posicion 4"},
        {id: 5, text: "Posicion 5"},
        {id: 6, text: "Posicion 6"},
        {id: 7, text: "Posicion 7"},
    ],
}

export default config;
