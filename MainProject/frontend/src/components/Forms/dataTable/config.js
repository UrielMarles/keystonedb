// Configuración predeterminada para el componente GenericTable
let config = {
    // Datos iniciales de ejemplo para la tabla
    Data: [{
            Column1Name: "Column1value",
            Column2Name: "Column2value"
        },
        {
            Column1Name: "Column1value",
            Column2Name: "Column2value"
        }
    ],
    // Estilo de la tabla
    Style: {
        color: "rojo", // Propiedad de color
        tamaño: "50"   // Propiedad de tamaño
    },
    // Configuración de la fuente de datos para la tabla
    Source: {
        Route: null, // Ruta de origen de datos (inicialmente nula)
        Params: {
            // Parámetros de la fuente de datos
            "columns": ["id, active, deletedAt"],
            "paging": {
                "itemsPerPage": 50,
                "actualPage": 1
            },
            "grouping": null,
            "labelValue": ["nombre", "id"],
            "order": ["nombre asc", "id desc"],
            "filters": [
                {
                    "field": "active",
                    "condition": "=",
                    "value": true,
                    "conector": "and"
                },
                {
                    "field": "deletedAt",
                    "condition": "=null",
                    "value": null,
                    "conector": "and"
                },
            ]
        }
    }
}

// Exporta la configuración predeterminada
export default config;
