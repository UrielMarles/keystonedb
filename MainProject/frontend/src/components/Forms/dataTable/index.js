// Importaciones de React y componentes de Material-UI
import React, { useEffect, useState } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

// Importación de la configuración predeterminada desde el archivo 'config'
import configDefault from './config';

// Definición del componente funcional 'GenericTable'
export default function GenericTable({ opts }) {
  // Estado para almacenar la configuración de la tabla, inicializado con la configuración predeterminada y opcionalmente con las opciones proporcionadas
  const [config, setConfig] = useState(Object.assign(configDefault, opts));
  // Estado para almacenar los datos que se cargarán en la tabla, inicializado en null
  const [data, setData] = useState(null);

  // Imprimir la configuración en la consola para propósitos de depuración
  console.log(config);

  // Efecto que se ejecuta al montar el componente
  useEffect(() => {
    // Verifica si la ruta de origen (config.Source.Route) no es nula
    if (config.Source.Route !== undefined) {
      // Realiza una solicitud de datos utilizando la ruta y parámetros de origen
      fetch(config.Source.Route, { method: 'POST', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(config.Source.Params) })
        .then(response => response.json())
        .then(data => {
          // Actualiza el estado de los datos y la configuración con los datos cargados
          setData(data);
          setConfig({ ...config, ...{ "Data": data } });
        });
    }
  }, []); // La dependencia vacía garantiza que el efecto se ejecute solo al montar el componente

  // Obtener los encabezados de la tabla a partir de las claves del primer objeto en 'config.Data' o usar una clave temporal si no hay datos
  const headers = Object.keys(config.Data[0]) ? Object.keys(config.Data[0]) : ["llaveTemp"];

  // Renderizar la tabla utilizando componentes de Material-UI
  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          {/* Renderizar las filas de encabezado con los nombres de las columnas */}
          <TableRow>
            {headers.map((header) => (
              <TableCell key={header}>{header}</TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {/* Renderizar las filas de datos utilizando los nombres de las columnas y los datos */}
          {config.Data.map((row, index) => (
            <TableRow key={index}>
              {headers.map((header) => (
                <TableCell key={header}>{row[header]}</TableCell>
              ))}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};
