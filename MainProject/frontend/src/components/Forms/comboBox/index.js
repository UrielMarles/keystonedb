import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import configDefault from "components/Forms/comboBox/config.js"
import React, { useEffect, useState } from 'react';

export default function ComboBox({ opts }) {
  const [config, setConfig] = useState(Object.assign(configDefault,opts));
  const [data, setData]= useState(null)

  // Efecto que se ejecuta al montar el componente
  useEffect(() => {
    if(config.Source.Route !== null){
      fetch(config.Source.Route, {method: 'POST', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(config.Source.Params)})
      .then(response => response.json())
      .then(data => {setData(data) ;setConfig({...config,...{"Data": data}})} 
    )}
  }, []);
  
  return (
    <Autocomplete
      style={{ ...config.Style }}
      disablePortal
      id={config.Style.label}
      options={config.Data}
      sx={{ width: config.Visual ? config.Visual.width : "0px" }}
      renderInput={(params) => <TextField {...params} label={"prueba"} />}
    />
  );
}