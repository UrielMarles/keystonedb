// Configuración predeterminada para el componente ComboBox
let config = {
  // Datos que se cargarán en el ComboBox
  Data: [
    { "label": "opcion1", "value": "1" },
    { "label": "opcion2", "value": "2" },
    { "label": "opcion3", "value": "3" },
    { "label": "opcion4", "value": "4" }
  ],
  // Estilo del ComboBox
  Style: {
    "backgroundColor": "white",
    "label": "textoTitulo",
    "width": "50%"
  },
  // Origen de los datos para el ComboBox
  Source: {
    // Ruta de la API para cargar datos (actualmente nula, se debe especificar la ruta real)
    Route: null,
    // Parámetros para la solicitud de datos
    Params: {
      "columns": ["id", "nombre", "edad"],
      "paginacion": {
        "itemsXPagina": 50,
        "paginaActual": 1
      },
      "labelValue": ["nombre", "id"],
      "order": ['nombre asc', 'apellidodesc'],
      // Filtros para la solicitud de datos
      "filters": {
        "apellido": {
          "conector": "and",
          "condicion": "like",
          "tipo": "string",
          "valor": "%ponce%"
        },
        "nombre": {
          "conector": "or",
          "condicion": "=",
          "valor": "sheila"
        }
      }
    }
  }
};

export default config;
