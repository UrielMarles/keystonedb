// Configuración predeterminada para el componente ComboBox
let config = {
    Data:[
      {
        "createdAt": "05/12/2023 08:41",
        "id": 1,
        "name": "DEFAULT"
      },
      {
        "createdAt": "05/12/2023 08:41",
        "id": 2,
        "name": "ITEMS"
      },
      {
        "createdAt": "05/12/2023 08:41",
        "id": 3,
        "name": "CUANDO"
      },
      {
        "createdAt": "05/12/2023 08:41",
        "id": 4,
        "name": "FALTA"
      }
    ],
    Style: {
      "backgroundColor": "white",
      "label": "textoTitulo",
      "width": "70%",
      "height": 400,
      "defaultPageSize": 3,
      "pageSizeOptions": [5,3,2,1]
    },
    Source: {
      Route: null,
      TableName :"dummyTable",
      Params: {
        "columns": ["id", "nombre", "edad"],
        "labelValue": ["nombre", "id"],
        "order": ['nombre asc', 'apellidodesc'],
        "filters": {
          "apellido": {
            "conector": "and",
            "condicion": "like",
            "tipo": "string",
            "valor": "%ponce%"
          },
          "nombre": {
            "conector": "or",
            "condicion": "=",
            "valor": "sheila"
          }
        }
      }
    }
  };
export default config