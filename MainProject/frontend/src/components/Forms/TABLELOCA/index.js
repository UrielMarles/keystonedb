import { DataGrid} from '@mui/x-data-grid';
import React, { useEffect, useState } from 'react';
import configDefault from "components/Forms/TABLELOCA/config.js"
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import Box from '@mui/material/Box';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { SERVER_FETCH_URL, CLIENT_FETCH_URL } from 'config/http';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';

function combinarDiccionarios(diccionario2, diccionario1) {
  var resultado = diccionario1;
  for (var llave in diccionario2) {
    if (!(llave in resultado)) {
      resultado[llave] = diccionario2[llave];
    }
  }
  return resultado;
}

export default function DataTable({opts}) {
  
  const config = combinarDiccionarios(configDefault,opts);
  const visual = combinarDiccionarios(configDefault.Style,opts.Style);
  const serverMode = config.Source.Route !== undefined;
  const [rows, setRows]= useState(config.Data);
  const [columns, setColumns] = useState(config.Data.length > 0 ? Object.keys(config.Data[0]).map((key) => ({ field: key, headerName: key })) : []);
  const [queryBody, setQuery] = useState(config.Source.Params);
  const [queryResponse ,setQueryResponse] = useState(rows);
  const [updateNumber,setUpdateNumber] = useState(0)
  const [currentPage, setCurrentPage] = useState(0);
  const [pageSize, setPageSize] = useState(visual.defaultPageSize);
  const [rowCount, setRowCount] = useState(config.Data.totalItemsCount)
  const [selectionModel, setSelectionModel] = useState([]);
  const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);
  const [incorrectSelection, setIncorrectSelection] = useState(false)

  useEffect(() => {
      if(serverMode){
        fetch(config.Source.Route, {method: 'POST', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(queryBody)})
        .then(response => {/*console.log(response);*/return response.json()})
        .then(data => {
          if (data.items !== undefined){
          setColumns(data.items.length > 0 ? Object.keys(data.items[0]).map((key) => ({ field: key, headerName: key })) : []);
          setRows(data.items);
          setQueryResponse(data);
          setRowCount(data.totalItemsCount);
          }else{
            console.log(data)
          }
        }
      )}
    }, [queryBody,updateNumber]);

  useEffect(()=>{
    handlePageSizeChange(pageSize);
  },[])

  const openDeleteDialog = () => {
    setIsDeleteDialogOpen(true);
  };

  const closeDeleteDialog = () => {
    setIsDeleteDialogOpen(false);
  };

  const handleSortModelChange = (params) => {
    if(serverMode){
      let copiaQuery = {...queryBody};
      if (params !== undefined  && params.length > 0){
          
          copiaQuery["order"] = [`${params[0].field} ${params[0].sort}`]; 
          setQuery(copiaQuery);
      }else{
          delete copiaQuery["order"]
          setQuery(copiaQuery)
      }
    }
  }

  const handlePageSizeChange = (newPageSize) =>{
    setPageSize(newPageSize)   
    if (serverMode){
      let copiaQuery = {...queryBody};
      copiaQuery["paging"]= {
        "itemsPerPage":newPageSize,
        "actualPage":currentPage
      }
      setQuery(copiaQuery)
    }
  }


  const handlePageChange = (newPage) => {
    const paginaAumentada = newPage +1
    setCurrentPage(newPage)
    if(serverMode){
      console.log("entra aca")
      let copiaQuery = {...queryBody};
      copiaQuery["paging"]= {
        "itemsPerPage":pageSize,
        "actualPage":paginaAumentada
      }
      setQuery(copiaQuery)

    }
  }

  const activateSelection = () => {
    if (selectionModel.length > 0){
    if (serverMode){
    const selectedRows = rows.filter((row) => selectionModel.includes(row.id));
    selectedRows.forEach(row => {
      console.log(JSON.stringify({"id":row["id"],"active":true}))
      fetch(`${CLIENT_FETCH_URL}/${config.Source.TableName}`, {
        method: 'PUT',headers: {'Content-Type': 'application/json'},body:JSON.stringify({"id":row["id"],"active":true})
      }).then(response => {if (!response.ok) {throw new Error('Error activando');}return response.json();})
        .then(data => {console.log('Solicitud activar exitosa', data);})
        .catch(error => {console.error('Error:', error);});
    });
    setUpdateNumber(updateNumber+1)
    }
    setSelectionModel([]);} else{
      setIncorrectSelection(true);
    }
  }

  const deleteSelection = () => {
    if (serverMode){
    const selectedRows = rows.filter((row) => selectionModel.includes(row.id));
    selectedRows.forEach(row => {
      fetch(`${CLIENT_FETCH_URL}/${config.Source.TableName}/${row["id"]}`, {
        method: 'DELETE',headers: {'Content-Type': 'application/json'/* ACA IRIA EL TOKEN CUANDO LO PONGAMOS*/}
      }).then(response => {if (!response.ok) {throw new Error('Error en la solicitud DELETE');}return response.json();})
        .then(data => {console.log('Solicitud DELETE exitosa', data);})
        .catch(error => {console.error('Error:', error);});
    });
    setUpdateNumber(updateNumber+1)
    }else{
      const updatedRows = rows.filter((row) => !selectionModel.includes(row.id));
      setRows(updatedRows);
    }
    setSelectionModel([]);
    closeDeleteDialog();
  }
  const deactivateSelection = () => {
    if (selectionModel.length > 0){
    if (serverMode){
    const selectedRows = rows.filter((row) => selectionModel.includes(row.id));
    selectedRows.forEach(row => {
      fetch(`${CLIENT_FETCH_URL}/${config.Source.TableName}`, {
        method: 'PUT',headers: {'Content-Type': 'application/json'},body:JSON.stringify({"id":row["id"],"active":false})
      }).then(response => {if (!response.ok) {throw new Error('Error activando');}return response.json();})
        .then(data => {console.log('Solicitud desactivar exitosa', data);})
        .catch(error => {console.error('Error:', error);});
    });
    setUpdateNumber(updateNumber+1)
    }
    setSelectionModel([]);} else{
      setIncorrectSelection(true);
    }
  }

  return (<>
    <div style={{ height: visual.height, width: visual.width}}>
      <Box display="flex" flexDirection="row" sx={{ background: 'white', padding: 2, borderRadius: 2, border: '1px solid #ccc'}}>
        <IconButton
        sx={{ backgroundColor: 'gray',border:'1px solid #ccc' }}
         onClick={() => console.log(selectionModel[0],"primer id selected")}>
          <AddIcon sx={{ color: 'black' }}/>
        </IconButton>
        <Box sx={{ marginLeft: 1 }} /> 
        <IconButton
        sx={{ backgroundColor: 'gray',border:'1px solid #ccc' }}
         onClick={() => console.log('Icon button clicked')}>
          <EditIcon sx={{ color: 'black' }}/>
        </IconButton>
        <Box sx={{ marginLeft: 1 }} /> 
        <IconButton
        sx={{ backgroundColor: 'gray',border:'1px solid #ccc' }}
          onClick={openDeleteDialog}>
          <DeleteIcon sx={{ color: 'black' }}/>
        </IconButton>
        <Box sx={{ marginLeft: 1 }} /> 
        <IconButton
        sx={{ backgroundColor: 'gray',border:'1px solid #ccc' }}
          onClick={activateSelection}>
          <CheckIcon sx={{ color: 'black' }}/>
        </IconButton>
        <Box sx={{ marginLeft: 1 }} /> 
        <IconButton
        sx={{ backgroundColor: 'gray',border:'1px solid #ccc' }}
          onClick={deactivateSelection}>
          <CloseIcon sx={{ color: 'black' }}/>
        </IconButton>
      </Box>
      <DataGrid
        sx={{ background: 'white', borderRadius: 2, border: '1px solid #ccc'}}
        rows={rows}
        columns={columns}
        sorting= {true}
        pagination={true}
        sortingMode= {serverMode?"server":"client"}
        onSortModelChange={handleSortModelChange}
        rowsPerPageOptions = {visual.pageSizeOptions} //ofrece estos tamaños de pagina
        paginationMode = {serverMode?"server":"client"}
        page = {currentPage}
        pageSize ={pageSize}
        onPageChange = {(newPage) => handlePageChange(newPage)}
        onPageSizeChange={(newPageSize) => handlePageSizeChange(newPageSize)}
        selectionModel={selectionModel}
        onSelectionModelChange ={(newSelectionModel) => { setSelectionModel(newSelectionModel);} }
        rowCount = {rowCount}
        checkboxSelection = {true}
      />
      <Dialog open={isDeleteDialogOpen} onClose={closeDeleteDialog}>
        <DialogTitle>Eliminar</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {selectionModel.length > 0?"Seguro que queres eliminar las filas seleccionadas?":"no hay ninguna fila seleccionada"}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          {selectionModel.length > 0 &&(
          <Button onClick={closeDeleteDialog}>No</Button>)}
          <Button onClick={deleteSelection} autoFocus>
          {selectionModel.length > 0?"Sí":"OK"}
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog open={incorrectSelection} onClose={() => setIncorrectSelection(false)}>
        <DialogTitle>Intente nuevamente</DialogTitle>
        <DialogContent>
          <DialogContentText>
            No seleccionaste una cantidad de columnas validas
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setIncorrectSelection(false)} autoFocus>
          OK
          </Button>
        </DialogActions>
      </Dialog>
      
    </div>
    <button onClick={()=>{console.log(JSON.stringify(queryBody));console.log(JSON.stringify(queryResponse));}}>Imprimir JSON</button>
    </>
  );
}