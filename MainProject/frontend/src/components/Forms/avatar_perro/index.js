import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Stack from '@mui/material/Stack';


export default function ImageAvatars() {
  return (
    <Stack direction="row" spacing={2}>
      <Avatar alt="Jorge" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Ministro_Jorge_H._Ferraresi.jpg/600px-Ministro_Jorge_H._Ferraresi.jpg" sx={{ width: 56, height: 56 }}/>
      <Avatar alt="Magdalena" src="https://upload.wikimedia.org/wikipedia/commons/5/5c/Magdalena_Sierra.png" sx={{ width: 56, height: 56 }}/>
      <Avatar alt="Alejo" src="https://1.bp.blogspot.com/-L0Bsw4inPms/X62BoquEW7I/AAAAAAABEgI/uGjnUcDDLxQk-GL59BmmlhLEcoqw04jKgCNcBGAsYHQ/s1600/alejo%2Bavellaneda.jpg" sx={{ width: 56, height: 56 }}/>
    </Stack>
  );
}

