import { createSlice } from "@reduxjs/toolkit";
import { REDUX } from "./constants";

const initialState = {
  banderas: [],
};

export const slice = createSlice({
  name: REDUX.MI_BANDERA,
  initialState,
  reducers: {
    setBanderas: (state, action) => {
      state.banderas = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setBanderas } = slice.actions;

export default slice.reducer;
