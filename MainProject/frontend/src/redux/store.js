import { configureStore } from "@reduxjs/toolkit";
import miBanderaReducer from "redux/features/miBanderaSlice";
import { REDUX } from "./features/constants";

export const store = configureStore({
  reducer: {
    [REDUX.MI_BANDERA]: miBanderaReducer,
  },
});
