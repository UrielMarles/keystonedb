import {
  getLocalidades,
  registrar,
  verificarRegistrado,
  getBanderas,
  buscadorBanderas,
} from "./services";

export const Service = {
  getLocalidades: () => getLocalidades(),
  verificarRegistrado: (request) => verificarRegistrado(request),
  registrar: (request) => registrar(request),
  getBanderas: () => getBanderas(),
  buscadorBanderas: (filtro) => buscadorBanderas(filtro),
};
