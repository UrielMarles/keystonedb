import axios from "config/http";

export const getLocalidades = () =>
  axios.post("/generales/listar", {
    columnas: ["id"],
    labelValue: ["nombre", "id"],
    filtros: {
      activado: {
        conector: "and",
        condicion: "=",
        valor: true,
      },
      eliminadoEn: {
        conector: "and",
        condicion: "=null",
        valor: null,
      },
      "grupo.id": {
        conector: "and",
        condicion: "=",
        valor: 2003,
      },
    },
    orden: [],
  });

export const verificarRegistrado = (request) =>
  axios.post("/votantes/verificarRegistrado", request);

export const registrar = (request) =>
  axios.post("/votantes/registrarse", request);

export const getBanderas = () =>
  axios.post("/proyecto/obtenerPorTipo", { tipoId: 1001 });

export const buscadorBanderas = (filtro=null) =>
  axios.post("/proyecto/obtenerPorTipo", { tipoId: 1001, filtro });

