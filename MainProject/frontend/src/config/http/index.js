import axios from "axios";

const DATA_USER = "DATA_USER";
export const SERVER_FETCH_URL = "http://project-keystone-api:5000/" 
export const CLIENT_FETCH_URL = "http://localhost:5034/"

const api = axios.create();
api.defaults.baseURL = process.env.addressNavegador;

api.defaults.headers.common["Content-Type"] = "application/json";

api.interceptors.request.use(
  function (config) {
    // const user = localStorage.getItem(DATA_USER)
    //   ? JSON.parse(localStorage.getItem(DATA_USER))
    //   : null;

    // const token = user && user.token ? user.token : "";
    // config.headers["token"] = token;

    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

api.interceptors.response.use(
  function (response) {
    // Do something with response data
    if (
      response.data &&
      response.data.msg &&
      response.data.token &&
      response.data.msg === "Nuevo token."
    ) {
      const user = localStorage.getItem(DATA_USER)
        ? JSON.parse(localStorage.getItem(DATA_USER))
        : null;
      if (user) {
        user.token = response.data.token;
        localStorage.setItem(DATA_USER, JSON.stringify(user));
      }
      return api(response.config);
    }
    if (
      response.data &&
      response.data.msg &&
      response.data.msg.indexOf("Inicie sesion de nuevo") !== -1 // Si viene este texto
    ) {
      // Desloguear al usuario
      window.location.href = "/login/tokenCaducado";
      return;
    }

    return response;
  },
  function (error) {
    // Do something with response error
    return Promise.reject(error);
  }
);

export default api;
