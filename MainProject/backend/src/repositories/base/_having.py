import pdb
from turtle import pd
from src import db
from sqlalchemy.sql import null
import datetime
from src.controllers.base import commonTools


class Having():
    _field: None
    _options: None
    _entity: None
    _function: None
    _conditions = {}

    def __init__(self, query, field, function, options):
        # print('\n',query,'\n',entity,'\n',field,'\n',options)
        self._options = options
        self._field = field
        self._function = function
        self._dateFormat = '%d/%m/%Y'
        self._dateTimeFormat = '%d/%m/%Y %H:%M:%S'
        self._timeFormat = '%d/%m/%Y %H:%M:%S'
        self._conditions = {'=': self._equalTo, '>=': self._biggerOrEqualTo, '>': self._biggerThan, '<=':self._smallerOrEqualTo,'<':self._smallerThan,'!=':self._diferent, 
        '=null': self._isNull, '!null': self._isNotNull}
        if query is not None and not hasattr(query, "orsHaving"):
            setattr(query, "orsHaving", [])
        if query is not None and not hasattr(query, "andsHaving"):
            setattr(query, "andsHaving", [])
        self._query = query

    def apply(self):
        # si value es diccionario es por que tiene complejidad, ej: function
        value = None
        try:
            value = self._options["value"]
        except Exception as e:
            raise Exception(e)
            return jsonify({"error": True, 'msg': 'el field value no existe en el filtro'})
        if isinstance(value, dict) and "function" in value and "parameters" in value:
            # al momento el unico format disponible de value complejo {"function": "functionX", parameters:['param1', 'param2', 'param3',....]}
            # las functiones tiene que validar los parameters y tirar excepcion si la validacion es erronea
            # los parameters de las functiones deben ser implementados con kargs
            #method = None
            method = getattr(self, value['function'])
            parameters = value['parameters']
            method(self._entity, self._field, parameters)    
            # try:
            #     method = getattr(self, value['function'])
            # except Exception as e:
            #     raise Exception(e)
            #     return jsonify({"error": True, 'msg': 'el field value.function no existe en el filtro'})
            # try:
            #     parameters = value['parameters']
            # except Exception as e:
            #     raise Exception(e)
            #     return jsonify({"error": True, 'msg': 'el field value.parameters no existe en el filtro'})
            # method(self._entity, self._field, parameters)
        # sino es un value comun
        else:
            try:
                method = self._conditions[self._options['condition']]                
                # method = getattr(self, self._conditions[self._options['condition']])
            except Exception as e:
                raise Exception(e)
                return jsonify({"error": True, 'msg': 'el field condition no existe en el filtro'})
            try:
                method()
            except Exception as e:
                raise Exception(e)
                return jsonify({"error": True, 'msg': f"no existe la condition {self._options['condition']}"})
        return self._query

    def _equalTo(self):
        filtro = None
        if isinstance(self._options["value"], dict) and "entity" in self._options["value"] and "field" in self._options["value"]:                        
            entity = self._options["value"]["entity"]
            field = self._options["value"]["field"]
            value = self._availableColumns[f"{entity}.{field}"]            
            filtro = self._function == value
        date = None
        try:
            date = commonTools.turnToDateHour(self._options["value"])
            if isinstance(date, (datetime.datetime, datetime.date)):
                date = commonTools.turnToDateHour(self._options["value"])                
                filtro = self._function == date.strftime('%m-%d-%Y %H:%M:%S')
                return None
        except:
            pass
        if isinstance(self._options["value"], (int,float,bool)):
            filtro = self._function == self._options["value"]
        else:
            filtro = self._function  == f'{self._options["value"]}'
        if  self._options.get("conector", "and") == "or":                
            self._query.orsHaving.append(filtro)
        else:     
            self._query.andsHaving.append(filtro)

    # def _in(self):        
    #     field = getattr(self._entity, self._field)
    #     # print(field,'\n')
    #     self._query = self._query.filter(field.in_(self._options["value"]))    

    def _biggerOrEqualTo(self):
        filtro = None
        try:
            date = commonTools.turnToDateHour(self._options["value"])
            if isinstance(date, (datetime.datetime, datetime.date)):
                date = commonTools.turnToDateHour(self._options["value"])                
                filtro = self._function >= date.strftime('%m-%d-%Y %H:%M:%S')
                return None
        except:
            pass

        if isinstance(self._options["value"], (int,float,bool)):
            filtro = self._function >= self._options["value"]

        if  self._options.get("conector", "and") == "or":                
            self._query.orsHaving.append(filtro)
        else:     
            self._query.andsHaving.append(filtro)

    def _biggerThan(self):
        filtro = None
        try:
            date = commonTools.turnToDateHour(self._options["value"])
            if isinstance(date, (datetime.datetime, datetime.date)):
                date = commonTools.turnToDateHour(self._options["value"])                
                filtro = self._function  > date.strftime('%m-%d-%Y %H:%M:%S')
                return None
        except:
            pass
        if isinstance(self._options["value"], (int,float,bool)):
            filtro = self._function  > self._options["value"]

        if  self._options.get("conector", "and") == "or":                
            self._query.orsHaving.append(filtro)
        else:     
            self._query.andsHaving.append(filtro)

    def _smallerThan(self):
        filtro = None
        try:
            date = commonTools.turnToDateHour(self._options["value"])
            if isinstance(date, (datetime.datetime, datetime.date)):
                date = commonTools.turnToDateHour(self._options["value"])                
                filtro = self._function < date.strftime('%m-%d-%Y %H:%M:%S')
                return None
        except:
            pass
        if isinstance(self._options["value"], (int,float,bool)):
            filtro = self._function < self._options["value"]

        if  self._options.get("conector", "and") == "or":                
            self._query.orsHaving.append(filtro)
        else:     
            self._query.andsHaving.append(filtro)


    def _smallerOrEqualTo(self):
        filtro = None
        try:
            date = commonTools.turnToDateHour(self._options["value"])
            if isinstance(date, (datetime.datetime, datetime.date)):
                date = commonTools.turnToDateHour(self._options["value"])                
                filtro = self._function <= date.strftime('%m-%d-%Y %H:%M:%S')
                return None
        except:
            pass
        if isinstance(self._options["value"], (int,float,bool)):
            filtro = self._function <= self._options["value"]
                    
        if  self._options.get("conector", "and") == "or":                
            self._query.orsHaving.append(filtro)
        else:     
            self._query.andsHaving.append(filtro)

    def _diferent(self):
        filtro = None
        if isinstance(self._options["value"], (int,float,bool)):
            filtro = self._function != self._options["value"]
        elif isinstance(self._options["value"], str):
            filtro = self._function != f'{self._options["value"]}'

        if  self._options.get("conector", "and") == "or":                
            self._query.orsHaving.append(filtro)
        else:     
            self._query.andsHaving.append(filtro)

    def _isNull(self):
        filtro = self._function == null()
    
        if  self._options.get("conector", "and") == "or":                
            self._query.orsHaving.append(filtro)
        else:     
            self._query.andsHaving.append(filtro)

    # def _between(self):        
    #     desde = datetime.datetime.strptime(self._options["value"], '%d/%m/%Y')
    #     hasta = datetime.datetime.strptime(self._options["value2"], '%d/%m/%Y')
    #     self._query = self._query.filter(self._function.between(desde.strftime('%m/%d/%Y'), hasta.strftime('%m/%d/%Y')))  

    def _isNotNull(self):
        filtro = self._function != null()
        if  self._options.get("conector", "and") == "or":                
            self._query.orsHaving.append(filtro)
        else:     
            self._query.andsHaving.append(filtro)
"""
{
  "paginacion": {
    "itemsXPagina": 50,
    "paginaActual": 1
  },
  "order": ['name asc', 'apellidodesc'],
  "filtros": {
    "surname": {
      "conector": "and",
      "condition": "like",
      "tipo": "string"
      "value": "%ponce%"
    },
    "name": {
      "conector": "or",
      "condition": "=",
      "value": "sheila"
    }
    "edad": {
      "conector": "and ",
      "condition": ">=",
      "tipo": "entero"
      "value": "18"
    },
    "createdAt": {
      "conector": "and ",
      "condition": "=",
      "tipo": "date"
      "value": "01/01/2022"
    },
    "tipoUsuario": {
      "conector": "and ",
      "condition": "subquery",
      "value": {
        ['tipos', [{
          "id": {
      "conector": "and ",
      "condition": "in",
      "tipo": "entero"
      "value": "(1,2,3)"
    },    
        }]]      
      }
    }
  }
}
"""
