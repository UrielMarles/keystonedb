from flask import jsonify
from src import db
from src.repositories.base._baseRepository import BaseRepository
from src.entities.base.generalEntity import GeneralEntity
from src.entities.base.typeEntity import TypeEntity
#from src.entities.base.userEntity import UserEntity

class GeneralRepository(BaseRepository):
    returnJson = False
    _entities = {
        "general":(GeneralEntity,None),
        "depends":(GeneralEntity, "general.depends", "left"),
        "type": (TypeEntity, "general.type", "left"),
    }
    def __init__(self, data, returnJson=False):
        BaseRepository.__init__(self, data)
        self._includeColumns("general", "id, name, abreviatedName, auxInt1, auxInt2, auxString1, auxString2, auxDate1, auxDate2, dependsId, active, "+self.commonColumns)
        self._includeColumns("depends", "name, id")
