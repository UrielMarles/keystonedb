from flask import jsonify
from src import db
from src.repositories.base._baseRepository import BaseRepository
from src.entities.base.typeEntity import TypeEntity

class TypeRepository(BaseRepository):
    returnJson = False
    _entities = {
        "type":(TypeEntity,None),
    }
    def __init__(self, data, returnJson=False):
        BaseRepository.__init__(self, data)
        self._includeColumns("type", "id, auxString1, auxInt1, auxInt2, name, auxString2, active, "+self.commonColumns)