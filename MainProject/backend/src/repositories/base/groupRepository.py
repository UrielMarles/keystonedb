from flask import jsonify
from src import db
from src.entities.base.groupEntity import GroupEntity
from src.entities.base.userEntity import UserEntity
from src.entities.base.permissionEntity import PermissionEntity
from src.repositories.base._baseRepository import BaseRepository
import pdb

class GroupRepository(BaseRepository):
    _entities =  {
        "group": (GroupEntity, None),
        "users": (UserEntity,"group.users", "left"),
        "permissions": (PermissionEntity,"group.permissions", "left"),
        }
    
    def __init__(self, data, returnJson=False):
        BaseRepository.__init__(self , data)
        self._includeColumns("group", "id, name, active, "+ self.commonColumns)
        self._includeColumns("users"," id")
        self._includeColumns("permissions","id")


