from datetime import datetime, date
import pdb
from flask import jsonify, json
from src import db
from src.repositories.base._filter import Filter
from src.repositories.base._having import Having
from sqlalchemy.orm import aliased
from sqlalchemy.sql.expression import desc,asc, or_, text, true, and_
import logging

class BaseRepository():   
    _parameters = {}        
    __abstract__ = True
    _enabledColumns = []
    _exclude = False
    _availableColumns = {}
    _filters = []
    _returnJson  = False
    _order = []
    _paging = {}
    _query = None
    _entities = {}
    _enabledEntities = {}
    _aliases = {}
    _relaciones = []
    _grouping = []
    _columnsToPrint = []
    _help = False
    _groupingAplicado = False
    _id = None
    _labelValue = None
    _ors =[]
    _ands =[]
    _hors =[]
    _hands =[]
    commonColumns = "id, active, createdAt, createdById, modifiedAt, modifiedById, deletedAt, deletedById"

    def __init__(self, data, returnJson = False):
        self._parameters = {
        "columns": ["id, active, deletedAt"],
        "paging": {
            "itemsPerPage": 50,
            "actualPage": 1
        },
        "grouping": None,
        "labelValue": ["nombre","id"],
        "order": ["nombre asc","id desc"],
        "filters": [
            {
                "field": "active",
                "condition": "=",
                "value": True,                            
                "conector": "and"
            }, 
            {
                "field": "deletedAt",
                "condition": "=null",
                "value": None,                            
                "conector": "and"
            },]
        }
        self._id = data.get("id", None)
        self._labelValue = data.get("labelValue", None) 
        self._enabledColumns = self._nomalizeItemsString(data.get("columns", []))
        self._help = data.get("help", False) #no se usa
        self._exclude = data.get("exclude", False) #no se usa
        self._filters = data.get("filters", []) 
        self._order = data.get("order", [])
        self._grouping = data.get("grouping", None) #no se usa       
        self._paging = data.get("paging", None)        
        self._enabledEntities = {}
        self._returnJson = returnJson            
        self._relaciones = []
        self._columnsToPrint = []   
        self._calculatedColumns = []
        self._specialColumns = []
        self._availableColumns = {}
        self._aliases = {}           
        self._query = db.session.query()

    def _nomalizeItemsString(self, lista):
        i = 0                    
        for item in lista:
            lista[i] = item.strip() 
            i += 1
        return lista   

    def _parseField(self, tag):
        rel = tag.split(".", 1)
        #obtengo la primer clave del diccionario de aliases (es la entity printcipal)
        kEntity = list(self._aliases.keys())[0]
        field = rel[0]            
        if len(rel) == 2:
            kEntity = rel[0]            
            field = rel[1]   
        entity = None        
        try:
            entity =  self._aliases[kEntity]    
        except KeyError:
            raise KeyError(f"No existe el field {tag}")            

        return (entity, field)     

    def _applyEnabledEntity(self, tag):
        rel = tag.split(".", 1)
        #obtengo la primer clave del diccionario de aliases (es la entity main)
        kEntity = list(self._aliases.keys())[0]        
        if len(rel) == 2:
            kEntity = rel[0]            
        if self._enabledEntities.get(kEntity) is None:            
            self._enabledEntities[kEntity] = self._entities[kEntity] 
                  
    def applyColumns(self):        
        cols = []
        tags = []   
        if self._enabledColumns == []:
                self._enabledColumns = self._availableColumns
        if (self._exclude == False):
            for tag in self._enabledColumns:
                if tag.strip() in list(self._availableColumns.keys()):                        
                    self._applyEnabledEntity(tag)
                    cols.append(self._availableColumns[tag].label(tag))        
                    tags.append(tag)                                        
        else:
            for tag in list(self._availableColumns.keys()):
                if tag.strip() not in (self._enabledColumns + self._calculatedColumns):
                    self._applyEnabledEntity(tag) 
                    cols.append(self._availableColumns[tag].label(tag))    
                    tags.append(tag)
        self._columnsToPrint = tags
        self._query = db.session.query(*cols)       
        
    def applyLabelValue(self):
        cols = []
        tags = []        
        for tag in self._enabledColumns:         
                if tag.strip() in list(self._availableColumns.keys()) and tag.strip() not in self._calculatedColumns:                        
                    self._applyEnabledEntity(tag)

        label = self._labelValue[0]
        if label in self._availableColumns:
            cols.append(self._availableColumns[label].label('label'))   
            tags.append("label")                                        
        value = self._labelValue[1]
        if value in self._availableColumns:
            cols.append(self._availableColumns[value].label('value'))        
            tags.append("value")                                                    
        self._columnsToPrint = tags
        self._query = db.session.query(*cols)

    def _buildTag(self, alias, col):
        #la primer entity de _entities no lleva alias
        main = list(self._entities.keys())[0]            
        if alias.strip() == main:
            tag = col.strip()
        else:
            tag = alias.strip() +"." + col.strip()
        return tag

    def applyGrouping(self):
        pos = None        
        calculatedColumn = False
        for column in self._columnsToPrint:
            if column in self._calculatedColumns:
                calculatedColumn = True
        if calculatedColumn:
            columnsToGroup = []
            for column in self._columnsToPrint:
                if column not in self._calculatedColumns:
                    columnsToGroup.append(column)
            for calculated in self._calculatedColumns:    
                #si es labelValue el grouping solo es de label
                if self._labelValue != None:                
                    label = self._labelValue[0]
                    if label in self._availableColumns:
                        self._query = self._query.group_by(self._availableColumns[label])
                    value = self._labelValue[1]
                    if value in self._availableColumns:
                        self._query = self._query.group_by(self._availableColumns[value])
                else:
                    if (calculated in self._columnsToPrint):
                        pos = len(columnsToGroup)               
                        for i in range(pos):             
                            column = self._availableColumns[columnsToGroup[i]]
                            self._query = self._query.group_by(column)  
                        columnsEspecEnCalculadas = []
                        for column in self._specialColumns:
                            if column not in self._calculatedColumns and column not in columnsToGroup:
                                columnsEspecEnCalculadas.append(column) 
                        pos = len(columnsEspecEnCalculadas)
                        for i in range(pos):                 
                            column = self._availableColumns[columnsEspecEnCalculadas[i]]
                            self._query = self._query.group_by(column)

                        return None
                       
    def _includeColumns(self, aliasStr, columnsStr = '*', exclude = False):
        columns = self._nomalizeItemsString(columnsStr.split(","))
        i = 0        
        rel = aliasStr.strip()
        if self._aliases.get(rel) is None:            
            self.createAlias(rel)
        alias = self._aliases[rel]        
        if (columnsStr == "*"):
            for col in alias.__mapper__.columns.keys():
                    self._availableColumns[self._buildTag(aliasStr.strip(),col)] = getattr(alias, col.strip())
        else:   
            if (exclude == True):
                for col in alias.__mapper__.columns.keys():
                    if col.strip() not in columns:                                                
                        self._availableColumns[self._buildTag(aliasStr.strip(),col)] = getattr(alias, col.strip())
            else: 
                for col in columns:
                    if col.strip() in columns:                                                
                        self._availableColumns[self._buildTag(aliasStr.strip(),col)] = getattr(alias, col.strip())
  
    def _includeCalculatedColumn(self, tag, field):                
        self._availableColumns[tag.strip()] = field    
        self._calculatedColumns.append(tag.strip())

    def _includeSpecialColumns(self, tag, field):                
        self._availableColumns[tag.strip()] = field    
        self._specialColumns.append(tag.strip())

    def createAlias(self, rel):
        self._aliases[rel] = aliased(self._entities[rel][0], name=rel)   

    def createAliases(self):
        for rel in self._entities:
            self._aliases[rel] = aliased(self._entities[rel][0], name=rel)     

    def applyJoins(self):                         
        for rel in self._enabledEntities:
            if self._enabledEntities[rel][1] is not None:
                target = self._enabledEntities[rel][1].split(".")            
                #si se especifica el left join 
                if len(self._enabledEntities[rel]) == 3 and (self._enabledEntities[rel][2] == "left"):
                    self._query = self._query.join(self._aliases[rel], getattr(self._aliases[target[0]], target[1]), isouter=True)                    
                else: 
                    self._query = self._query.join(self._aliases[rel], getattr(self._aliases[target[0]], target[1]))

    def applyFilters(self):      
        if isinstance(self._filters, list):         
            for filter in self._filters:
                field = filter.get("field", None)
                if field in self._calculatedColumns:
                    function = self._availableColumns[field]
                    self._query = Having(self._query, field, function, filter).apply()              
                else:
                    self._applyEnabledEntity(field)
                    (entity, fieldStr) = self._parseField(field)
                    self._query = Filter(self._query, entity, fieldStr, filter).apply() 
            if len(self._filters) > 0 and hasattr(self._query, "ors") and hasattr(self._query, "ands"):
                self._query = self._query.filter(or_(and_(*self._query.ands),  or_(*self._query.ors)))
            if len(self._filters) > 0 and hasattr(self._query, "orsHaving") and hasattr(self._query, "andsHaving"):
                self._query = self._query.having(or_(and_(*self._query.andsHaving),  or_(*self._query.orsHaving)))
        else:
            for field in self._filters:
                if field in self._calculatedColumns:
                    function = self._availableColumns[field]
                    self._query = Having(self._query, field, function, self._filters[field]).apply()                    
                else:
                    self._applyEnabledEntity(field)
                    (entity, fieldStr) = self._parseField(field)
                    self._query = Filter(self._query, entity, fieldStr, self._filters[field]).apply() 
            if len(self._filters) > 0 and hasattr(self._query, "ors") and hasattr(self._query, "ands"):
                self._query = self._query.filter(or_(and_(*self._query.ands),  or_(*self._query.ors)))
            if len(self._filters) > 0 and hasattr(self._query, "orsHaving") and hasattr(self._query, "andsHaving"):
                self._query = self._query.having(or_(and_(*self._query.andsHaving),  or_(*self._query.orsHaving)))    

    def getAvailableColumns(self):
        lista = list(self._availableColumns)
        lista.sort()
        return lista

    def getHelp(self):
        help = dict()
        help["availableColumns"] = self.getAvailableColumns()
        return help

    def _generateQuery(self, show=None, _hide=[], _path=None):
        resultado = None 
        if self._labelValue != None:         
            self.applyLabelValue()   
        else:
            self.applyColumns()      
        self.applyFilters()
        self.applyJoins()     
        self.applyGrouping()
     
    def data(self, show=None, _hide=[], _path=None,labelValue=False, returnAsList=False):
        if self._id != None:
            return self.getEntityById()
        if self._help == True:
            return jsonify(self.getHelp())
        self._generateQuery(show,_hide,_path)
        if not self._order is None:
            self.applyOrder()
        else:
            entities =list(self._entities.keys())
            order = [{
                    "entity": entities[0],#La entity por la que se quiere orderar
                    "field":"id",# field por el que se quiere orderar
                    "des":False}, #si es true es descendente, else asc
                ]
            self._order = order
            self.applyOrder()
        if not self._paging is None:
            resultado = self.applyPaging()
            lista = self.serializePaging(resultado[0])
            return jsonify({'pageCount':resultado[1],'items':lista, 'totalItemsCount': resultado[2]})        
        resultado = self._query.all()        
        if labelValue:
            return resultado
        lista = self.serializePaging(resultado)
        if returnAsList:
            return lista            
        else: 
            return jsonify(lista)

    def one(self, show=None, _hide=[], _path=None,isJson=True):
        if self._help == True:
            return jsonify(self.getHelp())
        self._generateQuery(show,_hide,_path)
        resultado = self._query.first()
        if isJson == True:
            if resultado:
                return jsonify(self._itemToDict(resultado)),200
            else:
                return jsonify(msg="object not found", id=None), 200                
        else:
            if resultado: 
                return self._itemToDict(resultado)
            else: 
                return None

    def getEntityById(self):
        entity = None
        entities =list(self._entities.keys())
        main = entities[0]
        entity =  self._entities[main][0]
        query = db.session.query(entity)
        return query.filter_by(
            id = self._id, 
            deletedAt = None,
            deletedById = None
            ).first()

    def returnAsList(self, show=None, _hide=[], _path=None):
        self._generateQuery(show,_hide,_path)
        res= self._query.all()
        lista = []
        for item in res:
            if item[0] != None:
                lista.append(item[0])
        return lista

    def serializePaging(self, items):
        lista=[]
        for item in items:                        
            lista.append(
                self._itemToDict(item)
            ) 
        return lista

    def applyOrder(self):                
        for order in self._order:
            #separo el espacio por el name
            field = order.split(" ")            
            #el 2do elemento es el sentido del order - Ascendente o descendente
            try:
                sentido = field[1].lower()
            except IndexError:
                sentido = "asc"             
            if (field[0] not in self._calculatedColumns):
                entity, field = self._parseField(field[0].strip())    
                field = getattr(entity,field)
            else:
                field = "value"
            if sentido == "desc":
                self._query = self._query.order_by(desc(field))
            else:
                self._query = self._query.order_by(asc(field))

    def applyPaging(self):
        actualPage = None
        try:
            actualPage = self._paging["actualPage"]
        except Exception as e: 
            raise Exception(e)  
        itemsPerPage = None    
        try:
            itemsPerPage = self._paging["itemsPerPage"]
        except Exception as e: 
            raise Exception(e) 
        paginador = self._query.paginate(page=actualPage,per_page=itemsPerPage,error_out=False)
        return(paginador.items,paginador.pages, paginador.total)
    
    def _itemToDict(self, item, dateTimeFormat = "%d/%m/%Y %H:%M", dateFormat = "%d/%m/%Y"):
        registry = {}
        i = 0
        for tag in self._columnsToPrint:
            if isinstance(item[i] ,datetime):
                registry[tag] =  datetime.strftime(item[i], dateTimeFormat) 
            elif isinstance(item[i] ,date):
                registry[tag] =  datetime.strftime(item[i],dateFormat)
            else:
                registry[tag] = item[i]
                
            i += 1
        return registry
