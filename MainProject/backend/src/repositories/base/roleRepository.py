from flask import jsonify
from src import db
from src.entities.base.roleEntity import RoleEntity
from src.entities.base.userEntity import UserEntity
from src.entities.base.permissionEntity import PermissionEntity
from src.repositories.base._baseRepository import BaseRepository
import pdb

class RoleRepository(BaseRepository):
    _entities =  {
        "roles": (RoleEntity, None),
        "users": (UserEntity,"roles.users", "left"),
        "permissions": (PermissionEntity,"roles.permissions", "left"),
        }
    
    def __init__(self, data, returnJson=False):
        BaseRepository.__init__(self , data)
        self._includeColumns("roles", "id, name, active, "+self.commonColumns)
        self._includeColumns("users"," id")
        self._includeColumns("permissions","id, active, name")

