from flask import jsonify
from src import db
from src.repositories.base._baseRepository import BaseRepository
from src.entities.base.menuItemEntity import MenuItemsEntity
from src.entities.base.typeEntity import TypeEntity

class MenuItemsRepository(BaseRepository):
    returnJson = False
    _entities = {
        "menuItem":(MenuItemsEntity,None),
        "depends":(MenuItemsEntity, "menuItem.depends", "left"),
    }
    def __init__(self, data, returnJson=False):
        BaseRepository.__init__(self, data)
        self._includeColumns("menuItem", "label, url, position, "+self.commonColumns)
        self._includeColumns("depends", "label, id")