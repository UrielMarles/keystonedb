from re import A
from src.entities.base.userEntity import UserEntity
from src.entities.base.permissionEntity import PermissionEntity
from src.entities.base.roleEntity import RoleEntity
from src.entities.base.groupEntity import GroupEntity
from src.entities.base.tokenEntity import TokenEntity
# from src.entities.perfilEntidad import PerfilEntidad
# from src.entities.domicilioEntidad import DomicilioEntidad
# from src.entities.generalEntidad import GeneralEntity
from src import db
from sqlalchemy import func
from flask import jsonify
from src.controllers.base.security import tokenControllers
from src.repositories.base._baseRepository import BaseRepository
import logging


class UsersRepository(BaseRepository):
	returnJson = False
	_entities = {
        "user": (UserEntity, None),
        "roles": (RoleEntity, "user.roles", "left"),
        "permissions": (PermissionEntity, "user.permissions", "left"),
        "groups": (GroupEntity, "user.groups", "left"),
		"tokens": (TokenEntity, "user.tokens", "left"),
    }
	
	
	def __init__(self, data, returnJson=False):
		BaseRepository.__init__(self , data, returnJson)
		self._includeColumns("user", "id, username, email, name, surname, active, password, options, changePassword "+','+self.commonColumns)
		self._includeColumns("roles","id, name")
		self._includeColumns("permissions","id, name")
		self._includeColumns("groups","id")
		self._includeColumns("tokens","id, token")
		self._includeCalculatedColumn("apenom",func.concat(self._availableColumns['surname']," ",self._availableColumns['name']))
		self._includeCalculatedColumn("tag",
				func.concat(self._availableColumns['surname']," ",self._availableColumns['name'], "[", self._availableColumns['username'],"]"))
		self._includeCalculatedColumn("lowerUsername",func.lower(self._availableColumns['username']))
		self._includeCalculatedColumn("minPermit",func.min(self._availableColumns['permissions.id']))

		
	def hasPermit(self, id, permissions):
		permissions = db.session.query(
            PermissionEntity.name
        ).filter(
            PermissionEntity.deletedById == None, 
            PermissionEntity.deletedAt == None,
            PermissionEntity.active == True,
            UserEntity.id == id,
            PermissionEntity.name.in_( permissions )
        )
		permissions = self.filterPermissions(permissions,False)
		return len(permissions) > 0 

	def filterPermissions(self, permissions, serialize=True):
		permissionsList = []
		userPermissions = self.filterUserPermissions(permissions)
		logging.warning(userPermissions)	
		rolePermissions = self.filterRolePermissions(permissions)
		groupPermissions = self.filterGroupPermissions(permissions)
		permissions = userPermissions.union(rolePermissions).union(groupPermissions).all()
		if serialize:
			permissionsList = []
			for permission in permissions:
				permissionsList.append({
					"permission": permission[0]  
					})
			return jsonify(permissionsList)
		else:
			return permissions

	def filterUserPermissions(self, permissions):
		return permissions.join(
			UserEntity.permissions
        )

	def filterRolePermissions(self, permissions):
		return permissions.join(
		UserEntity.roles,
		RoleEntity.permissions
		)

	def filterGroupPermissions(self, permissions):
		return permissions.join(
		UserEntity.groups,
		GroupEntity.permissions
		)


	def getUserByToken(self, token):
		payload = tokenControllers.decodeToken(token)
		if not isinstance(payload, str):
			id = payload.get("id")
			user = UsersRepository({"id":id}).getEntityById()
			return jsonify({
				'userId': user.id,
				'email': user.email,
				'name': user.name,
				'surname': user.surname,
				'username': user.username,
				'options': user.options,
				'calle': user.perfil.domicilio.calle.name,
				"numeroPuerta": user.perfil.domicilio.puerta,
				"localidad":user.perfil.domicilio.localidad.name,
				"puerta":user.perfil.domicilio.puerta,
				"piso":user.perfil.domicilio.piso,
				"dptoLocal":user.perfil.domicilio.dptoLocal,
				"cp":user.perfil.domicilio.cp,
				"url": user.perfil.url,
				"observaciones": user.perfil.observaciones
				
				})
		return jsonify({"msg":"token not found", "error":True})

	# def obtenerAcccionesPermisos(self, token, data):
	# 	payload = tokenControllers.decodeToken(token)
	# 	userId = payload.get('id')
	# 	if self.tieneQueCambiarPassword(userId):
	# 		return jsonify({"status": "error","msg": "Cambiar password"})
	# 	else:
	# 		pantalla = data.get("pantalla")
	# 		module = data.get("module")
	# 		if pantalla and module:
	# 			permission =  "|"+module+"|"+pantalla+"|"+"%"
	# 			permissions = db.session.query(
	# 				PermissionEntity.name
	# 			).filter(
	# 				PermissionEntity.deletedById == None,
    # 		    	PermissionEntity.deletedAt == None,
    # 		    	PermissionEntity.active == True,
    # 		    	UserEntity.id == userId,
    # 		    	PermissionEntity.name.ilike(permission)
	# 			)
	# 			permissions = self.filterPermissions(permissions,False)
	# 			acciones = []
	# 			for permission in permissions:
	# 				accion = permission.name.split('|')[3]#El split devuelve ['','module','pantalla','accion','']
	# 				acciones.append(accion)
	# 				return jsonify({
    # 					"permissions": acciones
  	# 					})
	# 		else:
				# return jsonify({"estatus": True,"msg": "Falta pantalla o module"})


	# def tieneQueCambiarPassword(self, idUsuario):
	# 	user = db.session.query(UserEntity.changePassword).filter(UserEntity.id == idUsuario).first()
	# 	return user.changePassword

	# def obtenerPorMail(self, email):
	# 	return(db.session.query(
	# 		UserEntity
	# 	).filter(
	# 		UserEntity.email == email,
	# 		#UserEntity.active == True,
	# 		UserEntity.deletedById == None,
	# 		UserEntity.deletedAt == None
	# 	).first())


	
	# def listarAsignadosA(self):        		
	# 	self._parameters["labelValue"] = ["tag", "id"]
	# 	self._parameters["filters"].append({ "field": "id", "condition": ">", "value" : 0, "conector": "and" })
	# 	self._parameters["paging"] = None
	# 	#self._parameters["columns"] = ["tag", "id"]
	# 	self.__init__(data= self._parameters)
	# 	result = self.data(labelValue=True) 
	# 	return  result 