from src.entities.base.permissionEntity import PermissionEntity
from src.entities.base.userEntity import UserEntity
from src.entities.base.groupEntity import GroupEntity
from src.entities.base.roleEntity import RoleEntity
from src.repositories.base._baseRepository import BaseRepository


class PermitRepository(BaseRepository):
    returnJson = False
    _entities = {
        "permission":(PermissionEntity, None),
        "users":(UserEntity, "permission.users", "left"),        
        "groups":(GroupEntity, "permission.groups", "left"),
        "roles":(RoleEntity, "permission.roles", "left"),        
    }

    def __init__(self, data, returnJson=False):
        BaseRepository.__init__(self, data)
        self._includeColumns("permission", "id, name, description, active, "+ self.commonColumns)
        self._includeColumns("users", "id, username")
        self._includeColumns("groups", "id")
        self._includeColumns("roles","id, name") 
        