from flask import jsonify
from src import db
from src.repositories.base._baseRepository import BaseRepository
from src.entities.base.applicationEntity import ApplicationEntity

class ApplicationRepository(BaseRepository):
    returnJson = False
    _entities = {
        "application":(ApplicationEntity,None),
    }
    def __init__(self, data):
        BaseRepository.__init__(self, data)
        self._includeColumns("application", "name, "+self.commonColumns)