from flask import jsonify
from src import db
from src.repositories.base._baseRepository import BaseRepository
from src.entities.project.dummyEntity import DummyEntity
import logging

class DummyRepository(BaseRepository):

    _entities = {
        "dummyTable":(DummyEntity,None)
    }
    def __init__(self, data, returnJson=False):
        BaseRepository.__init__(self, data, returnJson)
        self._includeColumns("dummyTable",self.commonColumns+", exampleInt, exampleString")

