def registerProjectModules(app):
    from src.modules.project.dummyModule import dummyModule
    app.register_blueprint(dummyModule)
