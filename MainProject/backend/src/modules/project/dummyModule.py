from flask import request
from src.entities.project.dummyEntity import DummyEntity
from src.repositories.project.dummyRepository import DummyRepository
from src.modules.base._baseModule import BaseModule
from src.controllers.base.security.tokenControllers import verifyPermission
from src.controllers.base import commonTools
from src.controllers.project.dummyControllers import sayHelloToPuppy

"""
Modules are the place where we define the routes of interaction that will be used in runtime, we define the 
URLS where our program will send requests, for example if the URL dummyhostname/users/id receives the info {"id":2} through
the DELETE method, the user with the id 2 will be deleted. All routes return a JSON with the info requested and a message
informing wether the intended outcome was succesfull code 200 means succes and 500 failure"
Once you understand how the routes are added to a module , you should read the notes in BaseModule to understand how a module
is initialized, the 5 common routes to all modules : create, modify, delete, list and obtain.

"""
customPermissions = {}
"""
I comment them for testing purposes
customPermissions = {"create":["|dummy|wanna|create|"],
                     "delete":["|dummy|mommy|delete|","|delete|daddy|permission|"]}
"""

"""
we specified permissions for the routes to create and delete, those routes will now verify those permissions, we
set enableAllPermissions to true, so the permissions to modify, list and get will be
|dummyTable|modify|,|dummytable|list|,|dummyTable|get| 
"""
dummyModule = BaseModule("dummyTable",DummyEntity,DummyRepository,customPermissions,enableAllPermissions=False).returnModule()

"""
this is an example route, we define the function that will be called when the route is called, the info
contained in the request (a json) can be accesed using request.get_json()
parts
dummyModule.route -> defines the route and the method that starts the function
verifyPermission -> this verifies that the user sending the request has the specified permissions, to fully understand permissions
read its notes in learningQuestions.txt
acceptJustJson -> verifies that the route receives a json, otherwise it raises an error
sayHelloToPuppy -> all routes defined in the modules should be as short as possible, to achieve this, we write the behaviour
of all custom routes in its own controllers file, in this case in controllers/project/dummyControllers , read the controllers file
to understand the /HelloPuppy custom route go read its controller in the mentioned folder
"""

@dummyModule.route("/dummy/HelloPuppy",methods=['GET'])
# @verifyPermission("|dummy|HelloPuppy")
@commonTools.acceptJustJSON
def sayHello():
    return sayHelloToPuppy()

"""
As explained in the learningQuestions section on Permissions, to use the route above us you need to be logged in, and the
user you are logged as needs to have the "|dummy|permission|" permission. To add a permission from start to a specific user
you can add it in data/base/csvs/permissions.csv
"""

