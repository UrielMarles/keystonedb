from flask import Blueprint, request
from src import db
from src.entities.base.typeEntity import TypeEntity
# from src.repositories.baselistaRepositorio import 

from src.controllers.base import commonTools
from src.controllers.base.security.tokenControllers import verifyPermission
import logging
import traceback



class BaseModule():
    """
    This module is used as a base to all modules, all modules are given the routes [create, delete, modify, list, get]
    """

    def __init__(self,tableName,entity,repository,customPermissions = {},enableAllPermissions = False):
        """
        Args:
           tableName(str): this name is used to name the routes and the table, EX: the delete route is called /tanleName/delete
           entity: this receives the entity to modify the table
           repository: this receives the repository to get info from the table
           enableAllPermissions(bool): if set to True, all routes with unspecified permission names will enable permissions using default permission names
           customPermissions(dict): it's used to modify the names of the permissions needed. the default permission name for each route is |name|route| an example could be |types|create|
           to use one or multiple specific permissions you need to specify its custom name in this dictionary
           EG: {"create":["|example|custom|permission|],"delete":["|custom|permission|deleteFirst|,"|custom|permission|deleteSecond"]}. 
        """
        self.entity = entity
        self.module = Blueprint(tableName,__name__,url_prefix = "/")
        self.repository = repository
        self.loadPermissions(customPermissions,enableAllPermissions,tableName)

        @self.module.route(tableName,methods=["POST"])
        @verifyPermission(self.createPermissions,self.createPermissionsEnabled)
        @commonTools.acceptJustJSON
        def create():
            return self.entity(request.get_json()).create()

        @self.module.route(tableName + "/<int:id>",methods=["DELETE"])
        @verifyPermission(self.deletePermissions,self.deletePermissionsEnabled)
        # @commonTools.acceptJustJSON
        def delete(id):
            resultado = self.repository({"id":id}).getEntityById()
            return resultado.delete()

        @self.module.route(tableName,methods=["PUT"])
        @verifyPermission(self.modifyPermissions,self.modifyPermissionsEnabled)
        @commonTools.acceptJustJSON
        def modify():
            return self.entity(request.get_json()).modify()
        
        @self.module.route(tableName+"/list",methods=["POST"])
        @verifyPermission(self.listPermissions,self.listPermissionsEnabled)
        #@commonTools.acceptJustJSON
        def list():
            return self.repository(request.get_json()).data()
        
        @self.module.route(tableName + "/<int:id>",methods=["GET"])
        @verifyPermission(self.getPermissions ,self.getPermissionsEnabled)
        #@commonTools.acceptJustJSON
        def obtain(id):
            resultado =self.repository({"id":id}).one() #esto se modifico para en vez de buscar el json obtenga el id que se pasa desde el URL
            return resultado # {"id":id} se pone asi porque lo que recibe la url es un entero y hay que parcearlo como un diccionario  

    def loadPermissions(self,customPermissions,enableAllPermissions,tableName):
        routes = ["create","delete","modify","list","get"]
        for routeName in routes:
            setattr(self,f"{routeName}PermissionsEnabled",False)
            if customPermissions.get(routeName,False):
                setattr(self,routeName+"Permissions",customPermissions[routeName])
                setattr(self,f"{routeName}PermissionsEnabled",True)
            else:
                setattr(self,routeName+"Permissions",[f"|{tableName}|{routeName}|"])
                if enableAllPermissions:
                    setattr(self,f"{routeName}PermissionsEnabled",True)

    def returnModule(self):
        return self.module