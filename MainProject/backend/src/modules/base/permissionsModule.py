from flask import Blueprint, request
from src import db
from src.entities.base.permissionEntity import PermissionEntity
from src.repositories.base.permissionRepository import PermitRepository
from src.modules.base._baseModule import BaseModule
import logging
import traceback

permissionsModule = BaseModule("permissions",PermissionEntity,PermitRepository,enableAllPermissions=False).returnModule()