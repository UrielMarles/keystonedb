from flask import Blueprint, request
from src import db
from src.entities.base.typeEntity import TypeEntity
from src.repositories.base.typeRepository import TypeRepository
from src.modules.base._baseModule import BaseModule
import logging
import traceback

typesModule = BaseModule("types",TypeEntity,TypeRepository,enableAllPermissions=False).returnModule()

