from flask import Blueprint, request
from src import db
from src.entities.base.userEntity import UserEntity
from src.repositories.base.userRepository import UsersRepository 
from src.modules.base._baseModule import BaseModule
from src.controllers.base import commonTools
from src.controllers.base.security.tokenControllers import login
import logging
import traceback

url = "/users"
usersModule = BaseModule("users",UserEntity,UsersRepository,enableAllPermissions=False).returnModule()

@usersModule.route(url + "/login",methods=['POST'])
@commonTools.acceptJustJSON
def userLogin():    
    return login(request.get_json())


