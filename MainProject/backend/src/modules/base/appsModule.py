from src.entities.base.applicationEntity import ApplicationEntity
from src.repositories.base.applicationRepository import ApplicationRepository
from src.modules.base._baseModule import BaseModule

appsModule = BaseModule("applications",ApplicationEntity,ApplicationRepository,enableAllPermissions=False).returnModule()