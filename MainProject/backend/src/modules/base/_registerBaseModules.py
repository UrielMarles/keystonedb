def registerBaseModules(app):
    from src.modules.base.typesModule import typesModule
    from src.modules.base.generalsModule import generalsModule
    from src.modules.base.usersModule import usersModule
    from src.modules.base.rolesModule import rolesModule
    from src.modules.base.permissionsModule import permissionsModule
    from src.modules.base.groupsModule import groupsModule
    from src.modules.base.appsModule import appsModule
    from src.modules.base.menuItemsModule import menuItemsModule
    app.register_blueprint(typesModule)
    app.register_blueprint(generalsModule)
    app.register_blueprint(usersModule)
    app.register_blueprint(rolesModule)
    app.register_blueprint(permissionsModule)
    app.register_blueprint(groupsModule)
    app.register_blueprint(appsModule)
    app.register_blueprint(menuItemsModule)
    
    