from flask import Blueprint, request
from src import db
from src.entities.base.roleEntity import RoleEntity
from src.repositories.base.roleRepository import RoleRepository
from src.modules.base._baseModule import BaseModule
import logging
import traceback

rolesModule = BaseModule("roles",RoleEntity,RoleRepository,enableAllPermissions=False).returnModule()