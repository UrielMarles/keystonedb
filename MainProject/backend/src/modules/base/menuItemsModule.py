from src.entities.base.menuItemEntity import MenuItemsEntity
from src.repositories.base.menuItemRepository import MenuItemsRepository
from src.modules.base._baseModule import BaseModule

menuItemsModule = BaseModule("menuItems",MenuItemsEntity,MenuItemsRepository,enableAllPermissions=False).returnModule()