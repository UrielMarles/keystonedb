from src.entities.base.groupEntity import GroupEntity
from src.repositories.base.groupRepository import GroupRepository
from src.modules.base._baseModule import BaseModule

groupsModule = BaseModule("groups",GroupEntity,GroupRepository,enableAllPermissions=False).returnModule()