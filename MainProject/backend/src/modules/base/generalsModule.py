from src.entities.base.generalEntity import GeneralEntity
from src.repositories.base.generalRepository import GeneralRepository
from src.modules.base._baseModule import BaseModule

generalsModule = BaseModule("generals",GeneralEntity,GeneralRepository,enableAllPermissions=False).returnModule()