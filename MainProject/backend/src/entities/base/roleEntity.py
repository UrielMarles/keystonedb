from src import db
from sqlalchemy.orm import relationship
from src.entities.base._baseEntity import BaseEntity
from sqlalchemy import event

class RoleEntity(db.Model, BaseEntity):
    __tablename__ = "roles"
    id = db.Column(db.Integer, primary_key=True, nullable=True)                 
    name = db.Column(db.String(250), nullable=False)
    
    rolesPermissions = db.Table('rolesPermissions', db.Model.metadata,
        db.Column('roleId', db.Integer, db.ForeignKey('roles.id')),
        db.Column('permissionId', db.Integer, db.ForeignKey('permissions.id')),
        extend_existing=True
    )

    usersRoles = db.Table('usersRoles', db.Model.metadata,
        db.Column('userId', db.Integer, db.ForeignKey('users.id')),
        db.Column('roleId', db.Integer, db.ForeignKey('roles.id')),
         keep_existing=True
    )

    permissions = relationship(
        "PermissionEntity",
        secondary=rolesPermissions,
        back_populates="roles"
    )

    users = relationship(
        "UserEntity",
        secondary=usersRoles,
        back_populates="roles",
    )    

    def __init__(self, data = {}):   
        BaseEntity._init_(self, {},data)            
        self.name = data.get("name", None)
        
        
    _default_fields = [
        "id", "name", "users"        
    ]

    _hidden_fields = [
         "password", "changePassword"
     ]
    _readonly_fields = [    
    ]    

#validacion para insertar el registro    
@event.listens_for(RoleEntity, 'before_insert')
def eventoBIListener(mapper, connection, target):
    return None

#validacion para editar el registro    
@event.listens_for(RoleEntity, 'before_update')
def eventoBUListener(mapper,  connection, target):
    return None        

#alguna restriccion para borrar el registro    
@event.listens_for(RoleEntity, 'before_delete')
def eventoBDListener(mapper,  connection, target):
    return None