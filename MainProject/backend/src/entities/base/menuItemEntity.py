from src import db
from src.entities.base._baseEntity import BaseEntity
from src.controllers.base.commonTools import turnToDate
from sqlalchemy.orm import relationship
from sqlalchemy import event


class MenuItemsEntity(db.Model, BaseEntity):
    __tablename__ = "menuItems"
    id = db.Column(db.Integer, primary_key=True, nullable=True) 
    label = db.Column(db.String(250), nullable=False)
    url = db.Column(db.String(4000))
    position = db.Column(db.Integer)
    appId = db.Column(db.Integer, db.ForeignKey("applications.id"))
    isMenu = db.Column(db.Boolean)
    dependsId = db.Column(db.Integer, db.ForeignKey("menuItems.id"))
    depends = relationship("MenuItemsEntity",  remote_side=[id])

    _fieldsDict= ["id", "label","url"] 

    
    menuItemsPermissions = db.Table('menuItemsPermissions',  db.Model.metadata,
        db.Column('menuItemId', db.Integer, db.ForeignKey('menuItems.id')),
        db.Column('permissionId', db.Integer, db.ForeignKey('permissions.id')),
        keep_existing=True
    )

    permissions = relationship(
        "PermissionEntity",
        secondary=menuItemsPermissions,
        back_populates="menuItems")

    def __init__(self, data = {}):
        BaseEntity._init_(self, {},data) 
        self.name = data.get("name", None)
        self.url = data.get("url",None)
        self.label = data.get("url",None)
        self.typeId = data.get("typeId", None)
        self.dependsId = data.get("dependsId", None)


#validation before inserting the registry   
@event.listens_for(MenuItemsEntity, 'before_insert')
def eventoBIListener(mapper, connection, target):
    return None

#validation to edit the registry   
@event.listens_for(MenuItemsEntity, 'before_update')
def eventoBUListener(mapper,  connection, target):    
    return None        

#restrictions to delete a registry
@event.listens_for(MenuItemsEntity, 'before_delete')
def eventoBDListener(mapper,  connection, target):
    return None