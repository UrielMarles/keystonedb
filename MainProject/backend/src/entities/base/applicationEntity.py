from src import db
from sqlalchemy.orm import relationship
from src.entities.base._baseEntity import BaseEntity
from sqlalchemy import event

class ApplicationEntity(db.Model, BaseEntity):
    __tablename__ = "applications"
    id = db.Column(db.Integer, primary_key=True, nullable=True)                 
    name = db.Column(db.String(250), nullable=False)
    

    def __init__(self, data = {}):   
        BaseEntity._init_(self,data)            
        self.name = data.get("name", None)
        
        
    _default_fields = [
        "id", "name",         
    ]


#validacion para insertar el registro    
@event.listens_for(ApplicationEntity, 'before_insert')
def eventoBIListener(mapper, connection, target):
    return None

#validacion para editar el registro    
@event.listens_for(ApplicationEntity, 'before_update')
def eventoBUListener(mapper,  connection, target):
    return None        

#alguna restriccion para borrar el registro    
@event.listens_for(ApplicationEntity, 'before_delete')
def eventoBDListener(mapper,  connection, target):
    return None