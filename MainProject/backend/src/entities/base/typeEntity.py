from src import db
from sqlalchemy.orm import validates
from src.entities.base._baseEntity import BaseEntity
from sqlalchemy import event

class TypeEntity(db.Model, BaseEntity):
    __tablename__ = "types"    
    id = db.Column(db.Integer, primary_key=True, nullable=True)             
    name = db.Column(db.String(250), nullable=False)    
    auxString1 = db.Column(db.String(250))
    auxString2 = db.Column(db.String(250))
    auxInt1 = db.Column(db.String(250))
    auxInt2 = db.Column(db.String(250))
    
    def __init__(self, data = {}):   
        BaseEntity._init_(self, {},data)           
        
        self.name = data.get("name", None)
        self.auxString1 = data.get("auxString1", None)
        self.auxString2 = data.get("auxString2", None)
        self.auxInt1 = data.get("auxInt1", None)

        
#validation before inserting the registry   
@event.listens_for(TypeEntity, 'before_insert')
def eventoBIListener(mapper, connection, target):
    return None

#validation to edit the registry   
@event.listens_for(TypeEntity, 'before_update')
def eventoBUListener(mapper,  connection, target):    
    return None        

#restrictions to delete a registry
@event.listens_for(TypeEntity, 'before_delete')
def eventoBDListener(mapper,  connection, target):
    return None