import logging
from syslog import LOG_WARNING
from src import db
from flask import jsonify, request
from sqlalchemy import inspect
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm.attributes import QueryableAttribute
from sqlalchemy.sql.expression import not_
from psycopg2.errors import NotNullViolation, UniqueViolation
from src.controllers.base.commonTools import response_error,turnToDateHour
import datetime 
import json
import pdb
from sqlalchemy import Column, Boolean, DateTime, Integer, ForeignKey

class BaseEntity():
    """
    The BaseEntity is the blueprint that all entities use to share the common methods, columns and attributes

    CommonColumns: 
        active : the use of active is flexible. If something isn't active it doesn't mean it's deleted or that it shouldn't be counted. 
        createdAt : automatically asigned when the entry is made and can't be changed.
        createdById : automatically saves the ID (from users table) of the creator (default -1)
        createdBy : this defines to sqlalchemy the relationship between the creator and the entry allowing us to obtain not just the id but the complete object(entity) of the creator.
        modifiedAt : when a change is made (including deleting) the change time is saved here. (it overwrites the last time saved)
        modifiedById : this column starts empty and saves the id of the user who made the last change.
        modifiedBy : this column refers to the modifying user the same way as the "createdBy" column.
        deletedAt : when the "delete()" method is used the deletion time is saved here. The entry will continue existing but the program
        will ignore it. (logic delete)
        deletedById : works the same way as "modifiedById"
        deletedBy : works the same way as "modifiedBy"
    """

    ##Common Columns##
    id = db.Column(db.Integer, primary_key=True, nullable=True) 
    active = db.Column(db.Boolean)
    createdAt = db.Column(db.DateTime)
    modifiedAt = db.Column(db.DateTime)
    deletedAt = db.Column(db.DateTime)

    messageSuccesCreating = 'Object succesfully created'
    messageSuccesModding = 'Object succesfully modified'            
    messageSuccesDeleting ='Object succefully deleted'

    #These Common Columns need to be declared using @declared_attr because they contain foreign keys
    @declared_attr
    def createdById(cls):
        return db.Column(db.Integer, db.ForeignKey("users.id"))
    @declared_attr
    def createdBy(cls):    
        return relationship("UserEntity", foreign_keys="["+cls.__name__+".createdById]",  post_update=True)
    @declared_attr
    def modifiedById(cls):
        return db.Column(db.Integer, db.ForeignKey("users.id"))
    @declared_attr
    def modifiedBy(cls):
        return relationship("UserEntity", foreign_keys="["+cls.__name__+".modifiedById]",  post_update=True)
    @declared_attr
    def deletedById(cls):
        return db.Column(db.Integer, db.ForeignKey("users.id"))
    @declared_attr
    def deletedBy(cls):
        return relationship("UserEntity", foreign_keys="["+cls.__name__+".deletedById]",  post_update=True)

    def _init_(self, messages = {},data = {}):
        """
        When building a new Entity the response messages can be modified, to do so a dictionary specifying the messages
        can be used. Ex: {"MessageSuccesCreating:"example message","MessageSuccesDeleting":"i like deleting"}
        """
        self.id = data.get("id",None)
        self.active = data.get("active",None)
        self.messageSuccesCreating = messages.get("messageSuccesCreating",'Object succesfully created')
        self.messageSuccesModding = messages.get("messageSuccesModding",'Object succesfully modified')            
        self.messageSuccesDeleting = messages.get("messageSuccesDeleting",'Object succefully deleted')          
        self.createdAt = datetime.datetime.now()


    def create(self, singleTransaction = True, commonColumns = True):
        """ Creates a new entry in the corresponding table. If it's used in a Blueprint (module) that doesn't verify the 
        user through token, the default value to "createdById" will be -1 (God).
        Args:
            singleTransaction(bool): if set to False the change is delayed until a db.session.commit()
            commonColumns(bool): if set to False the columns that are automatically asigned values when creating a table 
            (createdAt, createdById) aren't assigned values.
        Returns:
            (string,int) it returns a string with the succes/failure message (jsonified) and an int with its corresponding 
            number code touse as a response
        """
        if request:
            token = request.headers.get('token')
            if token:
                from src.controllers.base.security.tokenControllers import getUserIdWithToken
                self.createdById = getUserIdWithToken(token)
        try:
            if commonColumns:
                if self.createdById is None:
                    self.createdById = -1            
                self.createdAt = datetime.datetime.now()
            db.session.add(self) 
            db.session.flush()   
            if singleTransaction != False:                 
                db.session.commit()                                            
        except (IntegrityError, AssertionError, ValueError) as err:
            db.session.rollback()
            try:
                raise err.orig
            except (NotNullViolation, UniqueViolation) as err:    
                return response_error(err)                        
        else:
            return jsonify(msg=self.messageSuccesCreating, id=self.id), 200


    def modify(self, singleTransaction = True):
        """
        Modifies the contents of an entity. If it's used in a Blueprint (module) that doesn't verify the user through token, 
        the default value to "modifiedById" will be -1 (God).
        Args:
            singleTransaction(bool): if set to False the change is delayed until a db.session.commit()
        Returns:
            (string,int) it returns a string with the succes/failure message (jsonified) and an int with its corresponding code to
            use as a response
        """
        try:       
            if request:
                token = request.headers.get('token')
                from src.controllers.base.security.tokenControllers import getUserIdWithToken
                self.modifiedById = getUserIdWithToken(token)      
            entity = self.__class__
            registry = db.session.query(entity).get(self.id)
            if self.modifiedById is None:
                self.modifiedById = -1
            self.modifiedAt = datetime.datetime.now()            
            try: 
                self.updateValues(registry)            
            except AttributeError:
                  return response_error("No existe el object")    
            if singleTransaction != False:
                db.session.flush()          
                db.session.commit()                      
        except (IntegrityError, AssertionError, ValueError) as err:
            db.session.rollback()
            try:
                raise err.orig
            except (NotNullViolation, UniqueViolation) as err:    
                return response_error(err)                        
        else:
            return jsonify(msg=self.messageSuccesModding, id=self.id), 200

    def deactivate(self, singleTransaction = True):
        """
        Sets the "active" field of an entity to False, this will also update modifiedAt and modifiedBy(if there isnt a logged user the id will be -1)
        Args:
            singleTransaction(bool): if set to False the change is delayed until a db.session.commit()
        Returns:
            (string,int) it returns a string with the succes/failure message (jsonified) and an int with its corresponding code to
            use as a response
        """         
        self.active = False
        return self.modify(singleTransaction)
    
    def activate(self, singleTransaction = True):
        """
        Sets the "active" field of an entity to True, this will also update modifiedAt and modifiedBy
        (if there isnt a logged user the id will be -1)
        Args:
            singleTransaction(bool): if set to False the change is delayed until a db.session.commit()
        Returns:
            (string,int) it returns a string with the succes/failure message (jsonified) and an int with its corresponding code to
            use as a response
        """         
        self.active = True
        return self.modify(singleTransaction)

    def delete(self, singleTransaction = True):
        """
        This sets the "deletedBy" and "deletedAt" fields to the corresponding user and time. (if there isnt a logged user the id will be -1)
        this is considered a logical delete. For the program to properly work all interactions with the table must check that
        these fields are empty.
        Args:
            singleTransaction(bool): if set to False the change is delayed until a db.session.commit()
        Returns:
            (string,int) it returns a string with the succes/failure message (jsonified) and an int with its corresponding code to
            use as a response
        """
        try:      
            if request:
                token = request.headers.get('token')
                from src.controllers.base.security.tokenControllers import getUserIdWithToken
                self.deletedById = getUserIdWithToken(token)      
            entity = self.__class__
            registry = db.session.query(entity).get(self.id)            
            if self.deletedById is None:
                self.deletedById = -1
            registry.deletedById = self.deletedById
            registry.deletedAt = datetime.datetime.now()
            if singleTransaction != False:
                db.session.flush() 
                db.session.commit()                               
        except (IntegrityError, AssertionError, ValueError) as mensaje: 
            db.session.rollback()
        else:                
            return jsonify(msg=self.messageSuccesDeleting, id=self.id), 200

    def updateValues(self, instance):
        """
        This method is used to apply the values in an entity to another entity. If the first entity doesn't specify
        the value of a column this column isnt overwritten in the second entity. 
        Args:
            instance : this contains the entity that we are updating
        """
        for i in inspect(self).attrs:
            if not (i.value is None) and i.value != []:
                setattr(instance, i.key, i.value)        


    def toDict(self, show=None, _hide=[], _path=None ,dateTimeFormat = "%d/%m/%Y %H:%M:%S", dateFormat = "%d/%m/%Y"):
        """
        This method returns a dictionary with the corresponding names and values of a row
        Args:
            show(list): this argument specifies wich columns to include in the dictionary, if left empty the columns included
            will be those specified in _fieldsDict (this field should be in each entity)
            _hide(list): this argument specificies wich columns should not be included in the dictionary (this is mainly used when
            we want to remove some fields from _fields dict but leave the rest). This argument doesn't overwrite the columns defined
            as hidden in _hidden_fields, it just adds more columns.
            _path(str) = defines the prefix used in the dictionary for each field, if not specified it will be tablename in lowercase
            dateTimeFormat,dateFormat = dates in the database will be shown using these formats 
        """
        hidden = self._hidden_fields if hasattr(self, "_hidden_fields") else []
        default = self._fieldsDict if hasattr(self, "_fieldsDict") else []
        show = show or default
        if not _path:
            _path = self.__tablename__.lower()
            def prepend_path(item):
                item = item.lower()
                if item.split(".", 1)[0] == _path:
                    return item
                if len(item) == 0:
                    return item
                if item[0] != ".":
                    item = ".%s" % item
                item = "%s%s" % (_path, item)
                return item
            _hide[:] = [prepend_path(x) for x in _hide]
            show[:] = [prepend_path(x) for x in show]
        columnsNames = self.__table__.columns.keys()
        relationships = self.__mapper__.relationships.keys()
        properties = dir(self)
        ret_data = {}
        for key in columnsNames:
            if key.startswith("_"):
                continue
            check = "%s.%s" % (_path, key)
            # check = key
            if check in _hide or key in hidden:
                continue
            if check.lower() in show:
                if isinstance(getattr(self, key) ,datetime.datetime):
                    ret_data[key] =  datetime.datetime.strftime(getattr(self, key), dateTimeFormat)
                elif isinstance(getattr(self, key) ,datetime.date):
                    ret_data[key] =  datetime.datetime.strftime(getattr(self, key),dateFormat)    
                else: 
                    ret_data[key] = getattr(self, key)
        for key in relationships:           
            if key.startswith("_"):
                continue
            check = "%s.%s" % (_path, key)
            check = check.lower()
            if check in _hide or key in hidden:
                continue                
            if (check in show) or (key in default):                                
                _hide.append(check)
                is_list = self.__mapper__.relationships[key].uselist
                if is_list:
                    items = getattr(self, key)
                    if self.__mapper__.relationships[key].query_class is not None:
                        if hasattr(items, "all"):
                            items = items.all()
                    ret_data[key] = []
                    for item in items:
                        ret_data[key].append(item.toDict())
                else:
                    if (self.__mapper__.relationships[key].query_class is not None or self.__mapper__.relationships[key].instrument_class is not None):
                        item = getattr(self, key)
                        if item is not None:
                            ret_data[key] = item.toDict()
                        else:
                            ret_data[key] = None
                    else:
                        ret_data[key] = getattr(self, key)
        for key in list(set(properties) - set(columnsNames) - set(relationships)):
            if key.startswith("_"):
                continue
            if not hasattr(self.__class__, key):
                continue
            attr = getattr(self.__class__, key)
            if not (isinstance(attr, property) or isinstance(attr, QueryableAttribute)):
                continue
            check = "%s.%s" % (_path, key)
            if check in _hide or key in hidden:
                continue
            if check in show or key in default:
                val = getattr(self, key)
                if hasattr(val, "toDict"):
                    ret_data[key] = val.toDict()
                else:
                    try:
                        ret_data[key] = json.loads(json.dumps(val))
                    except:
                        pass
        return ret_data
    
    def fromDict(self, dateTimeFormat = "%d-%m-%Y %H:%M:%S", dateFormat = "%d-%m-%Y", **kwargs):
        """
        This method is used when we want to give an entity the information from a dictionary, the entity could be empty
        Ex: UserEntity({}).fromDict(dictionary) or it could be used to update the info in an existing entity. The dictionary
        can contain DateTime Objects, lists or numbers. Relationships will be asigned if necessary.
        Args:
            dateTimeFormat,dateFormat: If a field is a DateTime object this will specify the format of the parsed string
            **Kwargs: this can be used in two ways, firstly we can submit the fields with the info in a dictionary using
            the syntax **dictionaryName or we can give each field as a different argument. Ex: (id = 1,name ="sofia",friends = 0)
            in both ways this will be used to give the info to the entity.
        """
        _force = kwargs.pop("_force", False)
        readonly = self._readonly_fields if hasattr(self, "_readonly_fields") else []
        if hasattr(self, "_hidden_fields"):
            readonly += self._hidden_fields
        columns = self.__table__.columns
        columnsNames = columns.keys()
        relationships = self.__mapper__.relationships.keys()
        changes = {}
        for key in columnsNames:
            if key.startswith("_"):
                continue
            allowed = True if _force or key not in readonly else False
            exists = True if key in kwargs else False               
            if allowed and exists:
                val = getattr(self, key)                
                if val != kwargs[key]:                    
                    changes[key] = {"old": val, "new": kwargs[key]}
                    val = kwargs[key]
                    if columns[key].type.python_type == datetime.datetime and isinstance(val, str):                                            
                        f = turnToDateHour(val, dateTimeFormat, dateFormat)
                        val = f.strftime('%m-%d-%Y %H:%M:%S')
                    elif columns[key].type.python_type == datetime.date and isinstance(val, str):
                        f = turnToDateHour(val, dateTimeFormat, dateFormat)
                        val = f.strftime('%m-%d-%Y')
                    setattr(self, key, val)
        for rel in relationships:
            if key.startswith("_"):
                continue
            allowed = True if _force or rel not in readonly else False
            exists = True if rel in kwargs else False
            if allowed and exists:
                is_list = self.__mapper__.relationships[rel].uselist                
                if is_list:
                    valid_ids = []                    
                    cls = self.__mapper__.relationships[rel].argument()
                    for item in kwargs[rel]:           
                        valId = None
                        if isinstance(item, dict):                                     
                            if "id" in item:
                                valId = item["id"]
                        else: valId = item    
        
                        obj = db.session.query(cls).filter_by(id=valId).first()
                        if obj is not None:
                            valid_ids.append(obj)
                    setattr(self, rel, valid_ids)

    