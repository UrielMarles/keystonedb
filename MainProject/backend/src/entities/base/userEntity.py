import pdb
from src import db
from sqlalchemy.orm import relationship
from passlib.hash import pbkdf2_sha256
from src.entities.base._baseEntity import BaseEntity
from sqlalchemy import event


class UserEntity(db.Model, BaseEntity):
    __tablename__ = "users"  
    id = db.Column(db.Integer, primary_key=True, nullable=True) 
    username = db.Column(db.String(250), nullable=False)
    email = db.Column(db.String(250))
    name = db.Column(db.String(250))
    surname = db.Column(db.String(250))
    password = db.Column(db.String(250))
    token = db.Column(db.String(250))
    tokenTrimu = db.Column(db.String(250))
    options = db.Column(db.String(1000))
    isResponsible = db.Column(db.Boolean)
    changePassword = db.Column(db.Boolean)
    tokens = relationship("TokenEntity", back_populates="user", foreign_keys="[TokenEntity.userId]")
    __table_args__ = (db.UniqueConstraint("email", name='user_email'),db.UniqueConstraint("username", name="user_username"))

    usersPermissions = db.Table('usersPermissions', db.Model.metadata,
        db.Column('userId', db.Integer, db.ForeignKey('users.id')),
        db.Column('permissionId', db.Integer, db.ForeignKey('permissions.id')),
         keep_existing=True
    )

    usersGroups = db.Table('usersGroups', db.Model.metadata,
        db.Column('userId', db.Integer, db.ForeignKey('users.id')),
        db.Column('groupId', db.Integer, db.ForeignKey('groups.id')),
         keep_existing=True
    )

    usersRoles = db.Table('usersRoles', db.Model.metadata,
        db.Column('userId', db.Integer, db.ForeignKey('users.id')),
        db.Column('roleId', db.Integer, db.ForeignKey('roles.id')),
         keep_existing=True
    )

    permissions = relationship(
        "PermissionEntity",
        secondary=usersPermissions)
    groups = relationship(
        "GroupEntity",
        secondary=usersGroups)
    roles = relationship(
        "RoleEntity",
        secondary=usersRoles)        

    _fieldsDict= [
        "id", "username", "email", "name", "surname", "options", "permissions", 
    ]

    def __init__(self, data = {}):   
        BaseEntity._init_(self, {},data)           
        
        self.comercioId = data.get("comercioId", None)
        self.username = data.get("username", None)
        self.email = data.get("email", None)
        self.name = data.get("name", None)
        self.surname = data.get("surname", None)
        if data.get("password", None):
            self.password = pbkdf2_sha256.hash(data.get("password", None))
        self.changePassword = data.get("changePassword", None)
        self.tokenTrimu = data.get("tokenTrimu", None)
        self.token = data.get("token", None)
        self.options = data.get("options", None)        

#validation before inserting the registry   
@event.listens_for(UserEntity, 'before_insert')
def eventoBIListener(mapper, connection, target):
    return None

#validation to edit the registry   
@event.listens_for(UserEntity, 'before_update')
def eventoBUListener(mapper,  connection, target):    
    return None        

#restrictions to delete a registry
@event.listens_for(UserEntity, 'before_delete')
def eventoBDListener(mapper,  connection, target):
    return None

from src.entities.base.tokenEntity import TokenEntity