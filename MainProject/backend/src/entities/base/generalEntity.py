from src import db
from src.entities.base._baseEntity import BaseEntity
from src.controllers.base.commonTools import turnToDate
from sqlalchemy.orm import relationship
from sqlalchemy import event


class GeneralEntity(db.Model, BaseEntity):
    __tablename__ = "generals"
    id = db.Column(db.Integer, primary_key=True, nullable=True) 
    name = db.Column(db.String(250), nullable=False)
    abreviatedName = db.Column(db.String(250))
    auxString1 = db.Column(db.String(250))
    auxString2 = db.Column(db.String(4000))
    auxInt1 = db.Column(db.Integer)
    auxInt2 = db.Column(db.Integer)
    auxDate1 = db.Column(db.DateTime)
    auxDate2 = db.Column(db.DateTime)
    typeId = db.Column(db.Integer, db.ForeignKey("types.id"))
    dependsId = db.Column(db.Integer, db.ForeignKey("generals.id"))
    depends = relationship("GeneralEntity",  remote_side=[id])
    type = relationship("TypeEntity")

    _fieldsDict= [
        "id", "name"
        ] 

    def __init__(self, data = {}):
        BaseEntity._init_(self, {},data) 
        
        self.name = data.get("name", None)
        self.abreviatedName = data.get("abreviatedName", None)
        self.typeId = data.get("typeId", None)
        self.dependsId = data.get("dependsId", None)
        self.auxString1 = data.get("auxString1", None) 
        self.auxString2 = data.get("auxString2", None) 
        self.auxInt1 = data.get("auxInt1", None)
        self.auxInt2 = data.get("auxInt2", None) 
        self.auxDate1 = turnToDate(data.get("auxDate1"), None)
        self.auxDate2 = turnToDate(data.get("auxDate2"), None) 


#validation before inserting the registry   
@event.listens_for(GeneralEntity, 'before_insert')
def eventoBIListener(mapper, connection, target):
    return None

#validation to edit the registry   
@event.listens_for(GeneralEntity, 'before_update')
def eventoBUListener(mapper,  connection, target):    
    return None        

#restrictions to delete a registry
@event.listens_for(GeneralEntity, 'before_delete')
def eventoBDListener(mapper,  connection, target):
    return None