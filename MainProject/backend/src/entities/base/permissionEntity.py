from src import db
from sqlalchemy.orm import validates, relationship
from src.entities.base._baseEntity import BaseEntity
from sqlalchemy import event
from sqlalchemy.ext.declarative import declarative_base

class PermissionEntity(db.Model, BaseEntity):
    __tablename__ = "permissions" 
    id = db.Column(db.Integer, primary_key=True, nullable=True) 
    name = db.Column(db.String(250), nullable=False)
    description = db.Column(db.String(250))


    usersPermissions = db.Table('usersPermissions', db.Model.metadata,
        db.Column('userId', db.Integer, db.ForeignKey('users.id')),
        db.Column('permissionId', db.Integer, db.ForeignKey('permissions.id')),
         keep_existing=True
    )

    rolesPermissions = db.Table('rolesPermissions',  db.Model.metadata,
        db.Column('roleId', db.Integer, db.ForeignKey('roles.id')),
        db.Column('permissionId', db.Integer, db.ForeignKey('permissions.id')),
        keep_existing=True
    )

    groupsPermissions = db.Table('groupsPermissions',  db.Model.metadata,
        db.Column('groupId', db.Integer, db.ForeignKey('groups.id')),
        db.Column('permissionId', db.Integer, db.ForeignKey('permissions.id')),
        keep_existing=True
    )

    menuItemsPermissions = db.Table('menuItemsPermissions',  db.Model.metadata,
        db.Column('menuItemId', db.Integer, db.ForeignKey('menuItems.id')),
        db.Column('permissionId', db.Integer, db.ForeignKey('permissions.id')),
        keep_existing=True
    )

    users = relationship(
        "UserEntity",
        secondary=usersPermissions,
        back_populates="permissions")

    roles = relationship(
        "RoleEntity",
        secondary=rolesPermissions,
        back_populates="permissions")

    groups = relationship(
        "GroupEntity",
        secondary=groupsPermissions,
        back_populates="permissions")
    
    menuItems = relationship(
        "MenuItemsEntity",
        secondary=menuItemsPermissions,
        back_populates="permissions")

    _default_fields = [
        "id", "name" 
        ]   
    
    _hidden_fields = [
    
     ]
    _readonly_fields = [    
    ]    

    def __init__(self, data = {}):  
        BaseEntity._init_(self, {},data)               
        self.name = data.get("name", None)
        self.description = data.get("description", None)

#validacion para insertar el registro    
@event.listens_for(PermissionEntity, 'before_insert')
def eventoBIListener(mapper, connection, target):
    return None

#validacion para editar el registro    
@event.listens_for(PermissionEntity, 'before_update')
def eventoBUListener(mapper,  connection, target):
    return None        

#alguna restriccion para borrar el registro    
@event.listens_for(PermissionEntity, 'before_delete')
def eventoBDListener(mapper,  connection, target):
    return None