from src import db
from src.entities.base._baseEntity import BaseEntity
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Sequence
"""
los atributos que son del Tipo None no seras actualizados en la bd al momento de persistir
"""
Base = declarative_base()
class TokenEntity(db.Model, BaseEntity):
    __tablename__ = "tokens"     
    id = db.Column(db.Integer, primary_key=True, nullable=True)            
    token = db.Column(db.String(500), unique=True, nullable=False)    
    userId = db.Column(db.Integer, db.ForeignKey('users.id'))
    user = db.relationship("UserEntity" ,foreign_keys="[TokenEntity.userId]")

    def __init__(self, data = {}):  
        BaseEntity._init_(self, {},data) 
        
        self.token = data.get("token", None)
        self.userId = data.get("userId", None)

    @staticmethod
    def isUsed(token):
        # check whether auth token has been blacklisted
        res = db.session.query(TokenEntity).filter_by(token=str(token)).first()
        if res:
            return True  
        else:
            return False