from src import db
from sqlalchemy.orm import validates, relationship
from src.entities.base._baseEntity import BaseEntity
from sqlalchemy import event

class GroupEntity(db.Model, BaseEntity):
    __tablename__ = "groups"
    id = db.Column(db.Integer, primary_key=True, nullable=True) 
    name = db.Column(db.String(250), nullable=False)

    def __init__(self, data = {}): 
        BaseEntity._init_(self, {},data)    
        self.name = data.get("name", None)
    
    usersGroups = db.Table('usersGroups', db.Model.metadata,
        db.Column('userId', db.Integer, db.ForeignKey('users.id')),
        db.Column('groupId', db.Integer, db.ForeignKey('groups.id')),
         keep_existing=True
    )
    users = relationship(
        "UserEntity",
        secondary= usersGroups,
        back_populates="groups")    

    groupsPermissions = db.Table('groupsPermissions',  db.Model.metadata,
        db.Column('groupId', db.Integer, db.ForeignKey('groups.id')),
        db.Column('permissionId', db.Integer, db.ForeignKey('permissions.id')),
        keep_existing=True
    )

    permissions = relationship(
        "PermissionEntity",
        secondary=groupsPermissions,
        back_populates="groups")



#validacion para insertar el registro    
@event.listens_for(GroupEntity, 'before_insert')
def eventoBIListener(mapper, connection, target):
    return None

#validacion para editar el registro    
@event.listens_for(GroupEntity, 'before_update')
def eventoBUListener(mapper,  connection, target):
    return None        

#alguna restriccion para borrar el registro    
@event.listens_for(GroupEntity, 'before_delete')
def eventoBDListener(mapper,  connection, target):
    return None