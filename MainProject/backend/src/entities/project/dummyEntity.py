from src import db
from sqlalchemy.orm import validates
from src.entities.base._baseEntity import BaseEntity
from sqlalchemy import event



class DummyEntity(db.Model, BaseEntity):
    """
    Each entity represents a row in a table, be it existing or not. We can play with the entity and then apply those 
    changes to see them reflected. They also initialize their table the first time they are called, so its attributes
    will define the shape, columns and characteristics of the table. 
    """
    __tablename__ = "dummyTable"                
    id = db.Column(db.Integer, primary_key=True, nullable=True) 
    exampleMandatoryString = db.Column(db.String(250), nullable=False)    
    exampleString = db.Column(db.String(250))
    exampleInt = db.Column(db.Integer)

    #These messages can either be modified or deleted, it isn't necessary to add custom messages for each table
    #if they are let unspecified default messages will be used.
    messages = {
                "messageSuccesCreating":"Wow, your entry to the dummy table was succesully added",
                "messageSuccesModding": "Wow, the row in the dummy table was succesully modified",
                "messageSuccesDeleting": "Wow, the row in the dummy table was succesully deleted"
               }    

    
    def __init__(self, data = {}):
        """
        This is the builder of an entity, the entity can reflect an already existing row or one to be added.
        If it's already existing, the values in each property will be those of table, we can modify them
        and then submit the changes. If it doesn't exist we can define the values and then create the entry.
        When the BaseEntity is initialized the CommonColumns and methods will automatically be added to our entity
        """
        BaseEntity._init_(self, self.messages,data)
        self.exampleMandatoryString = data.get("exampleMandatoryString", None)
        self.exampleString = data.get("auxString2", None)
        self.exampleInt = data.get("auxInt1", None)

        
#validation before inserting the registry   
@event.listens_for(DummyEntity, 'before_insert')
def eventoBIListener(mapper, connection, target):
    return None

#validation to edit the registry   
@event.listens_for(DummyEntity, 'before_update')
def eventoBUListener(mapper,  connection, target):    
    return None        

#restrictions to delete a registry
@event.listens_for(DummyEntity, 'before_delete')
def eventoBDListener(mapper,  connection, target):
    return None