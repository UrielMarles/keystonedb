from src.entities.base.generalEntity import GeneralEntity
from src.entities.base.typeEntity import TypeEntity
from src.entities.base.roleEntity import RoleEntity
from src.entities.base.permissionEntity import PermissionEntity
from src.entities.base.groupEntity import GroupEntity
from src.entities.base.menuItemEntity import MenuItemsEntity
from src.entities.base.applicationEntity import ApplicationEntity

baseEntities=[
    TypeEntity,
    GeneralEntity,
    RoleEntity,
    GroupEntity,
    ApplicationEntity,
    MenuItemsEntity,
    PermissionEntity
    ]


