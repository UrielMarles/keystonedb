from src.entities.base.userEntity import UserEntity
from src.data.base.BaseData import baseEntities
from src.data.project.ProjectData import projectEntities
from src.controllers.base.commonTools import addEntrysFromCsv
import logging
TotalEntities = baseEntities + projectEntities
def load():
    addEntrysFromCsv("src/data/base/csvs/users.csv",UserEntity,"users",False)
    for entity in baseEntities:
        addEntrysFromCsv(f"src/data/base/csvs/{entity.__tablename__}.csv",entity,entity.__tablename__)
    for entity in projectEntities:
        addEntrysFromCsv(f"src/data/project/csvs/{entity.__tablename__}.csv",entity,entity.__tablename__)