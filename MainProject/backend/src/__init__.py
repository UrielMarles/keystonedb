from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from prometheus_flask_exporter import PrometheusMetrics
from src.modules.base._registerBaseModules import registerBaseModules
from src.modules.project._registerProjectModules import registerProjectModules
from  flask_mail  import  Mail
import logging

db = SQLAlchemy()
mail  = Mail()

def create_app(config="Config"):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object("config.{}".format(config))      
    cors = CORS(app)
    PrometheusMetrics(app, buckets=[0.1, 0.4, 0.7])

    registerBaseModules(app)
    registerProjectModules(app)

    from src.controllers.base import commonTools
    commonTools.route_error(app)
    mail.init_app(app)
    db.init_app(app)
    
    return app
