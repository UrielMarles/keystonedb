from src.scripts._herramientasComunes.funcionesGenericas import generarStringMinNum, convertirEnvStringABool
from src.repositories.baseseguridadUsuarioRepositorio import SeguridadUsuarioRepositorio
from src.repositories.baseseguridadPermisoRepositorio import PermitRepository
from src.repositories.baseroleesRepositorio import RoleRepository
from src.entities.base.userEntity import UserEntity
from src.repositories.baseseguridadGrupoRepositorio import GroupRepository
from src.repositories.basegeneralRepositorio import GeneralRepositorio
from src.repositories.baseperfilRepositorio import PerfilRepositorio
from src.datos.seguridad import constantes as constSeguridad
from src.scripts._mailer.mail import enviarMail
from src.scripts.seguridad import tokenScript
from flask import render_template_string
from passlib.hash import pbkdf2_sha256
from flask import jsonify, current_app
from os import environ
import pdb, logging
import  json
from src import db



def registrarUsuario(data, esCarga = False):    
    usuario = existeUsuario(data.get('username'),data.get('id', -2))
    if usuario:
        return jsonify({"error": True, "msg": f"Ya hay un usuario registrado con el username {data.get('username')}"})
    elif not (esCarga) and  emailYaRegistrado(data.get('email'),-2):
        return jsonify({"error": True, "msg": f"Ya hay un usuario registrado con el email {data.get('email')}"})
    data["changePassword"] = False     
    if data.get("id") != constSeguridad.SUPERUSUARIO_ID and esCarga != True:
        data['password'] = generarStringMinNum()
        data["changePassword"] = True
        # data["active"] = True
    usuario = crearUsuario(data)
    token = tokenScript.codificarToken(usuario.id)
    if not (esCarga):
        enviarMailBienvenida(data)
    return jsonify({
        "msg": "El usuario se registry exitosamente",
        "passwordAleatoria": data['password'],
        "token":token.decode()
    }),200

# def crearUsuario(data,serializar = False):
#     usuario = UserEntity({'active':True})
#     usuario.fromDict(**data)
#     usuario.crear()
#     if serializar:
#         return jsonify({"msg":usuario.mensajeCreadoOk,"id":usuario.id}),200
#     return usuario

def crearUsuario(data,serializar = False):
    #* Se agrega la validacion
    usuario = UserEntity(data)
    agregarRoles(data.get('roles'),usuario)
    agregarPermisos(data.get('permissions'),usuario)
    agregarGrupos(data.get('types'),usuario)
    agregarPerfil(data.get("perfilId"),usuario)
    usuario.crear()
    if serializar:
        return jsonify({"msg":usuario.mensajeCreadoOk,"id":usuario.id}),200
    return usuario

def agregarPerfil(perfilId,usuario):
    perfil = PerfilRepositorio({'id':perfilId}).obtenerEntidadPorId()
    if perfil:
        usuario.perfil = perfil
        db.session.commit()


def agregarRoles(roles,usuario):
    if roles:
        usuario.roles =[]
        for idRol in roles:
            role = RoleRepository({"id":idRol}).obtenerEntidadPorId()
            usuario.roles.append(role)

def agregarPermisos(permissions,usuario):
    if permissions:
        usuario.permissions=[]
        for idPermisos in permissions:
            permission = PermitRepository({"id":idPermisos}).obtenerEntidadPorId()
            usuario.permissions.append(permission)

def agregarGrupos(types,usuario):
    if types:
        usuario.types =[]
        for idGrupo in types:
            group = GroupRepository({"id":idGrupo}).obtenerEntidadPorId()
            usuario.types.append(group)

def eliminarUsuario(data):
    usuario = SeguridadUsuarioRepositorio(data).obtenerEntidadPorId()
    if not (usuario.roles or usuario.types or usuario.permissions):
        return UserEntity(data).eliminar()
    else:
        if usuario.roles:
            return jsonify({"error":True,"msg":"No se pudo eliminar el usuario porque tiene roles asociados"})
        elif usuario.permissions:
            return jsonify({"error":True,"msg":"No se pudo eliminar el usuario porque tiene permissions asociados"})
        else:
            return jsonify({"error":True,"msg":"No se pudo eliminar el usuario porque tiene types asociados"})

def modificarUsuario(data):
    id = data.get("id")
    email = data.get("email")
    username = data.get("username")
    usuario = SeguridadUsuarioRepositorio({"id": id}).obtenerEntidadPorId()
    if username is not None or email is not None:
        if existeUsuario(username,id):
            return jsonify({"error": True, "msg": f"Ya hay un usuario registrado con el username {username}"})
        elif emailYaRegistrado(email,id):
            return jsonify({"error": True, "msg": f"Ya hay un usuario registrado con el email {email}"})
    try:
        usuario.fromDict(**data)
    except AttributeError:
        logging.warning(f"El usuario con id {id} no existe")
        raise AttributeError(f"El usuario con id {id} no existe") 
    usuario.mensajeModificadoOk = 'El usuario se ha modificado correctamente'
    return usuario.modificar()



def modificarUsuarioPorToken(token,data):
   
    payload = tokenScript.decodificarToken(token)
    if not isinstance(payload,str):
        id= payload.get("id")
        usuario = SeguridadUsuarioRepositorio({"id":id}).obtenerEntidadPorId()

        if usuario.username != data.get("username"):
            if existeUsuario(usuario.username,id):
                return jsonify({"error": True, "msg": "Ya hay un usuario registrado con ese username"})
            else:
                usuario.fromDict(**data)
                usuario.mensajeModificadoOk= " el usuario se modifico correctamente "
                return usuario.modificar()


def existeUsuario(username,id):
    
    parameters = {
        "columnas":["id"],
        "excluir":False, 
        "paginacion": None, 
        "filtros": {
            "username":{
                "conector": "and",
                "condition": "like",
                "value": username,
            },
            "id":{
                "conector": "and",
                "condition": "!=",
                "value": id,
            },
            
        },
        "order":[]
    }
    repo = SeguridadUsuarioRepositorio(parameters)
    entity = repo.one(isJson= False)      
    return True if entity else False

def emailYaRegistrado(email,id):
    parameters = {
                "columnas":["id"],
                "excluir":False, 
                "paginacion": None, 
                "filtros": {
                    "email":{
                        "conector": "and",
                        "condition": "=", 
                        "value": email,
                    },
                    "id":{
                        "conector": "and",
                        "condition": "!=",
                        "value": id,
                    },
                    
                },
                "order":[]
            }
    #pdb.set_trace()
    entity = SeguridadUsuarioRepositorio(parameters).one(isJson= False)
    return True if entity else False


def modificarPassword(token,data):
    payload = tokenScript.decodificarToken(token)
    if not isinstance(payload,str):
        id = payload.get('id')
        usuario = SeguridadUsuarioRepositorio({"id":id}).obtenerEntidadPorId()
        if usuario and pbkdf2_sha256.verify(data.get('confirmacion'),usuario.password):
            usuario.password = pbkdf2_sha256.hash(data.get('nuevaContraseña', None))
            usuario.changePassword = False
            return usuario.modificar()
        else:
            return jsonify({"msg":"Contraseña incorrecta","error":True})
    return jsonify({"error":True,"msg":payload})


def reiniciarContrasenia(data):
    id = data.get('id')
    password = generarStringMinNum()
    resp = UserEntity({'id': id, 'password':password, 'changePassword': True}).modificar()
    if json.loads(resp[0].data.decode()).get("id"): #Verifico que se haya modificado
        # usuario = SeguridadUsuarioRepositorio({'id':id}).obtenerEntidadPorId()
        return jsonify({"nuevaPassword":password})


def olvidarContrasenia(data):
    parameters = {
            "columnas":["surname","id","name"],
            "excluir":False, 
            "paginacion": None, 
            "filtros": {
                "email":{
                    "conector": "and",
                    "condition": "=",
                    "value": data.get('email'),
                },
                "username":{
                    "conector": "and",
                    "condition": "=",
                    "value": data.get('username'),
                },
            },
            "order":[]
        }
    usuario = SeguridadUsuarioRepositorio(parameters).one(isJson= False)
    if usuario:

        password = generarStringMinNum()
        UserEntity({"id":usuario.get('id'),"password":password}).modificar()
        return jsonify({'msg': 'Password reiniciada',"password":password}),200
    else:
        parameters = {
            "columnas":["id"],
            "excluir":False, 
            "paginacion": None, 
            "filtros": {
                "email":{
                    "conector": "and",
                    "condition": "=",
                    "value": data.get('email'),
                },
            },
            "order":[]
        }
        usuario = SeguridadUsuarioRepositorio(parameters).one(False)
        if usuario:
            return {'error': True, 'msg': 'Username incorrecto '}
        else:
            return {'error': True, 'msg': 'Mail incorrecto '}

def enviarPasswordMail(data):
    datosMail = GeneralRepositorio({}).obtenerPorNombreAbreviado("MAIL_ENVIO_CLAVE")
    linkLogin = GeneralRepositorio({}).obtenerPorNombreAbreviado("WEB_LOG_IN")
    secretaria = GeneralRepositorio({}).obtenerPorNombreAbreviado("NOMBRE_SEC")
    logo = GeneralRepositorio({}).obtenerPorNombreAbreviado("URL_LOGO_UNDAV")
    usuario = SeguridadUsuarioRepositorio({}).obtenerPorMail(data.get('email'))
    html = datosMail.auxString2 if datosMail else None

    if datosMail and html:
        asunto = datosMail.auxString1
        linkLogin = linkLogin.auxString1 if linkLogin else None
        secretaria = secretaria.auxString1 if secretaria else None
        logo = logo.auxString1 if logo else None
        if usuario:
            envioDeMailDesactive = convertirEnvStringABool(environ.get("MAIL_SUPPRESS_SEND"))
            if envioDeMailDesactive:
                return {'error': True, 'msg': 'El envio de mail se encuentra desactive, modifique MAIL_SUPPRESS_SEND'}
            password = generarStringMinNum()
            UserEntity({'id': usuario.id, 'password':password, 'changePassword': True}).modificar()
            html = render_template_string(
                html,
                password = password,
                username = usuario.username,
                name = usuario.name,
                linkLogin = linkLogin,
                secretaria = secretaria,
                logo = logo
            )
            enviarMail(asunto,[usuario.email],html)
        else:
            return {'error': True, 'msg': 'No se encuentra usuario con ese mail'}
    else:  
        logging.warning('No hay datos de mail "MAIL_CAMBIO_CLAVE"')
        return {'error': True, 'msg': 'No hay datos de mail "MAIL_CAMBIO_CLAVE"'}
    return {'msg': 'Se envio el mail con la nueva contraseña'}

def enviarMailBienvenida(data):
    envioDeMailDesactive = current_app.config["MAIL_SUPPRESS_SEND"]
    datosMail = GeneralRepositorio({}).obtenerPorNombreAbreviado("MAIL_BIENVENIDA")
    linkLogin = GeneralRepositorio({}).obtenerPorNombreAbreviado("WEB_LOG_IN")
    secretaria = GeneralRepositorio({}).obtenerPorNombreAbreviado("NOMBRE_SEC")
    logo = GeneralRepositorio({}).obtenerPorNombreAbreviado("URL_LOGO_UNDAV")
    html = datosMail.auxString2 if datosMail else None
    if datosMail and html:
        asunto = datosMail.auxString1
        linkLogin = linkLogin.auxString1 if linkLogin else None
        secretaria = secretaria.auxString1 if secretaria else None
        logo = logo.auxString1 if logo else None
        if not envioDeMailDesactive:
            html = render_template_string(
            html,
            password = data.get("password"),
            username = data.get("username"),
            name = data.get("name"),
            linkLogin = linkLogin,
            secretaria = secretaria,
            logo = logo
            )
            enviarMail(asunto,[data.get("email")],html)
        else: 
            logging.warning('No se puede enviar mail de registry de usuario, modifique MAIL_SUPPRESS_SEND')
    else:
        logging.warning('No se puede enviar mail de registry de usuario, no hay datos de mail "MAIL_BIENVENIDA')

# def olvideContrasenia(data):
#     username = data.get('username')
#     mail = data.get('email')
#     user =  db.session.query(
#         UserEntity.id,
#         UserEntity.name,
#         UserEntity.surname,
#     ).filter(
#         UserEntity.email == mail,
#         UserEntity.username == username
#     ).first()
#     if user:
#         password = generarStringMinNum()
#         UserEntity({'id':users.id, 'password': password}).modificar()
#         from .mailReiniciarPasswordScript import enviarReinicioMail
#         name = user.name if user.name else ""
#         surname = user.surname if user.surname else ""        
#         usuario = {
#             'nameCompleto': surname + ' ' + name,
#             'username':username,
#             'password': password,
#             'email': mail
#         }
#         enviarReinicioMail(usuario)
#         return {
#             'msg': 'Password reiniciada'
#         }
#     else:
#         user =  db.session.query(
#             UserEntity.id
#         ).filter(
#             UserEntity.username == username
#         ).first()
#         if user:
#             return {'error': True, 'msg': 'Mail incorrecto '}
#         else:
#             return {'error': True, 'msg': 'Username incorrecto '}