import logging
from src import db
from flask import jsonify, request, current_app
from src.entities.base.userEntity import UserEntity
from src.entities.base.tokenEntity import TokenEntity
from src.repositories.base.userRepository import UsersRepository
import datetime
from os import environ
from src.controllers.base.commonTools import genStringMinNum
import jwt
from passlib.hash import pbkdf2_sha256
from functools import wraps


def encodeToken(userId):
    code = genStringMinNum()           
    minVencimiento = int(environ.get("MIN_VENC_TOKEN"))
    payload = {
        'exp': datetime.datetime.utcnow() + datetime.timedelta(days=0, minutes=minVencimiento, seconds=0),
        'iat': datetime.datetime.utcnow(),
        'id': userId,
        'code': code        
    }

    TokenEntity({"token": code, "userId": userId}).create()
    UserEntity({"id": userId, "token": code}).modify()
    return jwt.encode(
        payload,
        environ.get("SECRET_KEY"),
        algorithm='HS256'
    )

def regenToken(payload):
    minRegen = int(environ.get("MIN_REG_TOKEN"))
    iat = payload.get("iat")
    exp = payload.get("exp")
    expiryTime = datetime.datetime.fromtimestamp(exp)
    regenTime = datetime.datetime.fromtimestamp(iat) + datetime.timedelta(days=0, minutes=minRegen, seconds=0)    
    actualTime = datetime.datetime.now()    
    id = payload.get("id")
    if regenTime < actualTime: 
        token = encodeToken(id)
        respuesta = {
            'msg': 'Nuevo token.',
            'token': token.decode()
        }
        return jsonify(respuesta), 200
    return None   


def tokenExists(code):
    #logging.warning(code)
    parameters = {     
                "columns":["id"],
                "exclude":False,
                "paging": None,
                "filters": {
                    "tokens.token":{
                        "conector": "and",
                        "condition": "=",
                        "value": code
                    },
                    "deletedAt":{
                        "conector": "and",
                        "condition": "=null",
                        "value": None
                    },                    
                    "active":{
                        "conector": "and",
                        "condition": "=",
                        "value": True
                    } 
                },
                "order":[]
            }
    from src.repositories.base.userRepository import UsersRepository
    user = UsersRepository(parameters).one(isJson = False)  
    # logging.warning(user)  
    return False if user is None else True


def decodeToken(token):
    try:
        payload = jwt.decode(token,environ.get("SECRET_KEY") )
        if tokenExists(payload.get("code")):
            return payload
        else:
            return 'No existe token. Inicie sesion de nuevo.'    
    except jwt.ExpiredSignatureError:
        return 'Token caducado. Inicie sesion de nuevo.'
    except jwt.InvalidTokenError:
        return 'Token invalido. Inicie sesion de nuevo.'

def getUserIdWithToken(token):
    # token = request.headers.get('token')
    payload=decodeToken(token)
    if not isinstance(payload,str):
        return payload.get('id')
    
def login(data):    
    password = data.get("password")
    username = data.get("username")
    parameters = {     
                "columns":["id","password","name","surname","changePassword","username"],
                "exclude":False,                
                "paging": None,
                "filters": {
                    "username":{
                        "conector": "and",
                        "condition": "=",
                        "value": username
                    },
                    "deletedAt":{
                        "conector": "and",
                        "condition": "=null",
                        "value": None
                    },                    
                    "active":{
                        "conector": "and",
                        "condition": "=",
                        "value": True
                    } 
                },
                "order":[]
            }
    user = UsersRepository(parameters).one(isJson = False)
    
    if user == None:
        return jsonify({"msg":"El usuario no existe","error":True})
    elif user['changePassword'] == True:
        token = encodeToken(user['id'])
        return jsonify({"msg":"Must change password", "changePassword": True, "token" : token.decode()})
    elif pbkdf2_sha256.verify(password,user['password'] ):
        token = encodeToken(user['id'])
        if token:
            return({
                "msg":"Inicio de sesion exitoso",
                "name" : user['name'],
                "surname" : user['surname'],
                "token" : token.decode(),
                "changePassword": user['changePassword'],
                "username": user['username']
            }),200
    else:
        return jsonify({
            "error":True,
            "msg": "Contraseña incorrecta"
        }),200


def logout(token):
    if token:
        respuesta = decodeToken(token)
        if not isinstance(respuesta,str):
            return jsonify({'msg': 'Cierre session correctamente.'}),200
        else:
            return jsonify({"error":True,"msg":respuesta})
    else:
        return jsonify({"error":True,"msg":"Proporcione un token de autenticación valido."})

def verifyPermission(permissions=None,enableAllPermissions = True):
    @wraps(permissions)
    def decorador2(f):
        @wraps(f)
        def decorador(*args, **kwargs):
            if enableAllPermissions:
                token = None
                #Verifico que pasen token
                if 'token' in request.headers:
                    token = request.headers['token']
                if not token:
                    return jsonify({"error":True, 'msg': 'Falta token'})    
                #Descodifico y verifico que este bien  
                #decodificar verifica si hay un token activo y si no esta expirado
                payload =decodeToken(token)                                    
                #payloaf no es un string entonces va todo bien
                if not isinstance(payload, str):
                    respuesta = regenToken(payload)
                    if respuesta:
                        return respuesta
                else:    
                    return jsonify({"error":True, 'msg': payload})
                
                #Verifico que tenga permissions a la ruta, si no se pasa ningun permission puede acceder
                if permissions:
                    from src.repositories.base.userRepository import UsersRepository 
                    if not UsersRepository({}).hasPermit(payload.get('id'),permissions):
                        from src.controllers.base.commonTools import MissingPermissionsException
                        return jsonify({"error":True, 'msg': f"""Faltan permission, debe tener alguno:'{(', '.join(permissions))}'"""})                    
            return f(*args, **kwargs)
        return decorador
    return decorador2