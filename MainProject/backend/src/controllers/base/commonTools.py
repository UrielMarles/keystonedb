from secrets import choice
from string import  ascii_lowercase, digits
from flask import jsonify, request, make_response, abort
from functools import wraps
from datetime import datetime
from src import db
import re
import csv
import logging
from os import environ
from src import db
from passlib.hash import pbkdf2_sha256
import traceback
import json
import logging

def hasLetterAndNumber(string):
    return any(character in digits for character in string) and any(character in ascii_lowercase for character in string)


def genStringMinNum(length = 8):
    isCorrect = False
    characters = ascii_lowercase + digits
    while not isCorrect:
        password = ''.join(choice(characters) for character in range(length))
        isCorrect = hasLetterAndNumber(password)
    return password

def validateRegex(variable, validacion):
    return re.search(validacion, variable)

def areTheSame(x):
    return x.count(x[0]) == len(x)

def turnHtmlToString(path):
    try:
      file = open(path, "r")
      return (file.read())
    except IOError:
      return None
    
def turnEnvStringToBool(variable):
    env = variable.lower()
    if env == "true":
        return True
    elif env == "false":
        return False
    else:
        return ""

def activateProcessById (processList, entity):
    for processId in processList:
        try:
            data = {"id":processId}
            entity(data).activate(singleTransaction=False)
        except Exception as e:
            db.session.close()
            raise Exception(e)
    db.session.commit()
    return jsonify({"msg":"Se han active los procesos"})

def deactivateProcessById (processList, entity):
    for processId in processList:
        try:
            data = {"id":processId}
            entity(data).deactivate(singleTransaction=False)
        except Exception as e:
            db.session.close()
            raise Exception(e)
    db.session.commit()
    return jsonify({"msg":"Se han desactive los procesos"})

def activateByFilter(data, getFilter):
    filter = data.get('filter',"")
    objects = getFilter(filter)
    for object in objects:
        try:
            object.activate(singleTransaction=False)
        except Exception as e:
            db.session.close()
            raise Exception(e)
    db.session.commit()
    return jsonify({"error": False ,'msg': "Se han active los procesos"})

def deactivateByFilter(data,getFilter):
    filter = data.get('filter',"")
    objects = getFilter(filter)
    for object in objects:
        try:
            object.deactivate(singleTransaction=False)
        except Exception as e:
            db.session.close()
            raise Exception(e)
    db.session.commit()
    return jsonify({"error": False ,'msg': "Se han desactive los procesos"})

def csvToDictionary(route):
    import os

    data = []
    if os.path.isfile(route):
        with open (route, "r") as f:
            reader = csv.reader(f)
            firstRow = True
            fieldNames = []
            fieldsThatAreLists = []
            for row in reader:
                dictionary = {}
                j = 0
                if firstRow:
                    firstRow = False
                    for col in row:
                        if col.endswith("*"):
                            changedCol = col.replace("*","")
                            fieldNames.append(changedCol)
                            fieldsThatAreLists.append(j)
                        else:
                            fieldNames.append(col)
                        j = j + 1
                else:   
                    for col in row:
                        value = col
                        fieldName = fieldNames[j]
                        if col.lower() in ["true","false"]:
                            value = col.lower() == "true"
                        if col.startswith("!"):
                            colcode = col.replace("!","")
                            value = eval(colcode)
                        if j in fieldsThatAreLists:
                            listDict = []
                            listItems = col.split(",")
                            for item in listItems:
                                try:
                                    dict = {}
                                    dict["id"] = int(item)
                                    listDict.append(dict)
                                except:
                                    pass
                            dictionary[fieldName] = listDict
                        else:
                            dictionary[fieldName] = value
                        j = j + 1
                    data.append(dictionary)
    return data

def addEntrysFromCsv(route,entity,tag ="", commonColumns= True):
    data = csvToDictionary(route)
    try:
        for dictionary in data:
            newEntity = entity()
            if dictionary.get("password",None):
                dictionary["password"] = pbkdf2_sha256.hash(dictionary["password"])
            newEntity.fromDict(**dictionary)
            newEntity.create(singleTransaction = False,commonColumns = commonColumns)
            newEntity.active = True
    except Exception:
        db.session.rollback()
        logging.warning(traceback.format_exc())
        return response_error("the csv couldn't be added to the table, check if it's correctly formatted")
    db.session.commit()
    logging.warning(f"the entrys to the table {tag} have been succesfulyy added")
    return responseJson({"message":f"the csv info was succesfully added to the {tag} table"},200)

def route_error(app):
    @app.errorhandler(Exception)
    def exception_handler(e):
        logging.error(str(e))
        if app.config['FLASK_ENV'] == 'production':
            return {'error': True, 'msg': 'error'}
        else:
            return response_trace_error(str(e), 500)

class DuplicateValueException(Exception):
    def __init__(self, code=500, message='Error registry duplicado'):
        self.code = code
        self.message = message

class MissingFieldsException(Exception):
    pass

class MissingPermissionsException(Exception):
    pass

class MissingParametersException(Exception):
    pass

class DateFormatException(Exception):
    pass

class MissingRegistryException(Exception):
    pass

class FormatError(Exception):
    pass

class ValidationException(Exception):
    pass


def responseJson(obj, code=200):
    return jsonify(obj), code
    
def responseMessage(message, code=200):
    return responseJson({"mensaje": message}, code)

def responseNotFound(message = "No se encontró el recurso"):
    return responseMessage(message,404)

def response_error(err):
    abort(make_response(jsonify({"error": True, "message": str(err)}), 500))    

def response_trace_error(errors, code):
    return responseJson({"errores": errors, "trace": traceback.format_exc()}, code)

def responseBusinessError(errors):
    return response_trace_error(errors,400)
    
def acceptJustJSON(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if not request.is_json: 
            abort(415,'Solo se acepta JSON (Content-Type: application/json)')
        else:
            request.json_data = request.get_json()
        return func(*args, **kwargs)
    return wrapper

def verifyFields(requiredFields=[]):
    @wraps(requiredFields)
    def wrapper(func):
        @wraps(func)
        def wrapper2(*args, **kwargs):
            data = request.get_json()
            # Obtengo list claves del json de la peticion
            jsonKeys = data.keys()
            # Si el field obligatorio no esta en la list de jsonKeys agrego a fields faltantes
            missingFields = list( filter( lambda mandatoryField: not (mandatoryField in jsonKeys) , requiredFields) )
            # Si hay field faltante lanzo excepcion, sino sigo con la peticion
            if len(missingFields):
                raise MissingFieldsException(f"""Faltan los fields '{(', '.join(missingFields))}'""")
            return func(*args, **kwargs)
        return wrapper2 
    return wrapper

def turnToDate(date = None, format = "%d-%m-%Y"):
    dateObj = None
    try:              
        if date != None:         
            date = date.replace("/", "-")        
            dateObj = datetime.datetime.strptime(date, format)
    except ValueError:
        response_error(f'Wrong format the correct format is: {format}')          
    return dateObj

def turnToDateHour(s, dateTimeFormat='%d-%m-%Y %H:%M:%S', dateFormat='%d-%m-%Y'):    
    try:        
        s = s.replace("/", "-")    
        s = datetime.strptime(s, dateTimeFormat)
    except:
        try:
            s = datetime.strptime(s, dateFormat)
        except: 
            try:
                s = datetime.strptime(s, "%H:%M:%S")
            except: 
                response_error(f"error en format date y/o hora debe ser {dateTimeFormat}")
    return s