
from flask import request
"""
Example usage:
(request send with GET method)
To read the info sent in the request use the method of request get_json() this returns a dictionary
{
    "puppyName":"robert"
}
it will return: 
{
    "msg":"your puppy robert it beatiful!"
}
if the puppy's name isnt specified it will return an error
"""
def sayHelloToPuppy():
    dictionaryWithInfo = request.get_json()
    if "puppyName" in dictionaryWithInfo:
        puppyName = dictionaryWithInfo["puppyName"]
        return {"msg":f"your puppy {puppyName} is beautiful!"},200
    else:
        return {"error":"You didn't give me your puppy's name D:"},500
