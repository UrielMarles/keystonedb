# Learning System Guide

Welcome to the learning experience of the inner workings of the system. This guide will provide you with an overview of key concepts and components within the system.

## Entities
Entities represent rows in a table, whether existing or not. They play a pivotal role in shaping and managing data. Here's what you need to know:

- **Definition**: An entity represents a row in a table and initializes its table when called for the first time. Attributes of an entity define the table's structure, columns, and characteristics.

- **Example Entity**: To understand entities better, explore the notes in `entities/project/dummyEntity` and `entities/base/_baseEntity`.

- **Notes**:
    - Initializing an entity with an existing row's ID does not load all row values into the entity. To achieve that, use the repository of the table.
    - Entities send or modify information in the table but do not read it.

## Modules
Modules define routes of interaction for runtime and specify the URLs where the program sends requests. Learn about modules:

- **Definition**: Modules define the routes of interaction for runtime and specify the URLs where the program sends requests.

- **Example of Route Usage**: For instance, if the URL `dummyhostname/users/id` receives `{"id":2}` via the DELETE method, user ID 2 will be deleted.

- **Example Module**: Refer to the notes in `modules/project/dummyModule` to gain insights. For common routes in all modules and how they work, consult the notes in `modules/base/_baseModule`.

- **Notes**:
    - All routes return a JSON with the requested information and a message indicating the success of the intended outcome.
    - Each module contains the scripts used in its routes iside the CONTROLLERS file.

## Repositories
Repositories are essential for retrieving information from the database. Here's what you need to know:

- **Definition**: Repositories are used to obtain information from the database. Information can be filtered based on specific criteria.

- **Example of Usage**: To retrieve all information from a certain row and store it in an entity, utilize the `getEntityById()` method.

- **Note**: Repositories exclusively used to read the table and do not modify it.

## Permissions
Permissions play a crucial role in controlling access to module routes. Here's what you need to know:

- **Definition**: Permissions allow users to access a module's route. While defining a route, the use of permissions is optional.

- **Notes**:
    - Permissions can be assigned to individual users, groups, or roles. Users inherit permissions from groups or roles they belong to.
    - Routes require users to be logged in to verify permissions. After logging in, users receive a token containing their information, which informs the route about the user's identity.
    - Tokens have expiration periods.

## How to Create a New Table
Creating a new table involves three key components: ENTITY, MODULE, and REPOSITORY. Follow these steps:

1. Create ENTITY, MODULE, and REPOSITORY files. You can use dummy examples as templates.
2. Register your MODULE in `modules/project/registerProjectModules` by importing it and using the `app.register_blueprint(yourModuleName)` method.
3. This registration is necessary because, during startup, all three components must be reached through an import chain. The server begins in `server.py`, where `create_app()` is called. Both `registerBaseModules()` and `registerProjectModules()` are invoked, and each module imports both the entity and the repository. Importing the entity for the first time creates the table with the defined columns.

## How to Add Data Since Start (Using CSVs)
You can add initial data to tables during system startup using CSV files. Follow these steps:

1. Create a CSV file with the desired columns and data. Place this CSV file in `data/project/csvs` and name it after the table.
2. Import the table's ENTITY and add its name to `data/project/ProjectData`.

**Notes when Creating the CSV**:
- To indicate a column with either a list or a one-to-many relationship, add an asterisk (*) at the end of the column name. List contents should be enclosed in quotes and separated by commas (`"2,3"`). If a single value exists in the same column in other rows, quotes are not necessary.
- In one-to-many or many-to-many relationships, list values should be the IDs of related rows.
- To add values with code to be executed, prefix the code with an exclamation mark (`!`). If the code uses a library, ensure the library is added to the commonTools controllers folder.
- To leave a column empty, use a comma to skip it or write "none".

**Example CSV Usage**: Refer to `data/project/csvs/dummyTable.csv` and `data/project/csvs/permissions.csv` to understand relationship assignments.

Enjoy your learning journey within the system! Feel free to explore and experiment with the provided examples and concepts.