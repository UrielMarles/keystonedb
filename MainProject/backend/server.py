from src import create_app, db
import logging
app = create_app()
if __name__ == "__main__":
    with app.app_context():
      db.drop_all()
      db.create_all()
      from src.data.DataLoader import load
      load()
    app.run(host="0.0.0.0", port=5000)
