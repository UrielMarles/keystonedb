from os import environ, path
from dotenv import load_dotenv
from src.controllers.base.commonTools import turnEnvStringToBool
basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, ".env"))


class Config:
    FLASK_APP = environ.get("FLASK_APP")
    FLASK_ENV = environ.get("FLASK_ENV")
    SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI")
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    ###Envio de mail
    MAIL_DEFAULT_SENDER = environ.get("MAIL_DEFAULT_SENDER")
    MAIL_PASSWORD = environ.get("MAIL_PASSWORD")
    MAIL_PORT = int(environ.get("MAIL_PORT"))
    MAIL_SERVER = environ.get("MAIL_SERVER")
    MAIL_USERNAME = environ.get("MAIL_USERNAME")
    MAIL_SUPPRESS_SEND = turnEnvStringToBool(environ.get("MAIL_SUPPRESS_SEND"))
    MAIL_USE_TLS =  True
    MAIL_USE_SSL = False

class ConfigTest(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI_TEST")
