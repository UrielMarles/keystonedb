# Postgresql
A continuacion se muestran los pasos necesarios para levantar un contenedor con una base de datos Postgresql .

## Procedimiento
Antes de comenzar, clonamos el repositorio.
>```sh
>$ git clone https://gitlab.com/CristianPastrana/postgres-elecciones-db.git
>```

Luego nos dirigimos al directorio:

>```sh
>$ cd postgres-elecciones-db
>```

"Buildeamos" la imagen.
>```sh
>$ docker-compose build
>```

Levantamos el contenedor.
>```sh
>$ docker-compose up
>```

Si todo fue bien, habremos iniciado el contenedor con el servicio de la base de datos.

IMPORTANTE: tener en cuenta que estamos usando los puertos que se utilizan por defecto, esto quiere decir que si tenemos instalado Postgresql de forma local puede haber conflictos con los puertos. Para solucionarlo debemos cambiar el puerto que queda expuesto en el contenedor. 

## Como accder

### database= elecciones

- Direccion: localhost

- Usuario : admin

- Contraseña :   pwd0123456789


### database= elecciones_tests

- Direccion: localhost

- Usuario : admin

- Contraseña :   pwd0123456789